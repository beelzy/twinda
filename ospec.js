const o = require('ospec');
const jsdom = require('jsdom');
const dom = new jsdom.JSDOM('', {url: process.env.TEST_DOMAIN, pretendToBeVisual: true});

global.window = dom.window;
global.document = dom.window.document;
global.requestAnimationFrame = dom.window.requestAnimationFrame;

require('mithril');

o.after(() => {
    dom.window.close();
});
