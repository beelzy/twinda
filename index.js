const compression = require('compression');
const express = require('express');
const ejs = require('ejs').__express;
const session = require('express-session');
const uuid = require('uuid');
const path = require('path');
const got = require('got');
const PORT = process.env.PORT || 5000
const helmet = require('helmet');

const app = express();
app.use(compression());
app.use(helmet());
app.use(express.json());
app.set('trust proxy', 1)
app.use(session({
    cookie: {
        secure: process.env.ENV !== 'DEV',
        httpOnly: true,
    },
    name: 'session',
    secret: process.env.EXPRESS_SECRET
}));

const accessRequired = {
    'deviation/metadata' : {query: ['ext_collection', '1']},
    'feed/notifications' : {},
    'user/whoami' : {}
}

const callbackUrl = (hostname, protocol) => {
    return `${protocol}://${encodeURIComponent(hostname)}${process.env.ENV === 'DEV' ? `:${PORT}` : ''}/callback`;
};

const token = async function(req, type='authorization_code', code='') {
    try {
        let response = await got(`https://www.deviantart.com/oauth2/token?grant_type=${type}&client_id=${process.env.CLIENT_ID}&client_secret=${process.env.DA_SECRET}&redirect_uri=${callbackUrl(req.hostname, req.protocol)}${code !== '' ? `&code=${code}` : ''}${type === 'refresh_token' ? `&refresh_token=${req.session.refresh_token}` : ''}`);
        let json = JSON.parse(response.body);
        if (json.hasOwnProperty('status') && json.status === 'success') {
            if (type === 'client_credentials') {
                req.session.client_token = json.access_token;
            } else {
                req.session.access_token = json.access_token;
                req.session.refresh_token = json.refresh_token;
            }
            req.session.type = type === 'client_credentials' ? (req.session.type || 'client') : 'user';
        }
        return json.status;
    } catch (e) {
        console.log('error requesting token', type, req.session);
        console.log(e.response.body);
    }
};

const api = async function(token, route, type, data={}) {
    data.append('access_token', token);
    try {
        let response = await got(`https://www.deviantart.com/api/v1/oauth2/${route}`, {searchParams: data});
        return {
            data: JSON.parse(response.body),
            type: type
        };
    } catch (e) {
        console.log('error accesing api', token, route, type);
        console.log(e.response.body);
        return {
            data: JSON.parse(e.response.body),
            type: type
        };
    }
};

const apiPost = async function(token, route, type, data) {
    try {
        data.access_token = token;
        let response = await got.post(`https://www.deviantart.com/api/v1/oauth2/${route}`, {form: data});
        return {
            data: JSON.parse(response.body),
            type: type
        };
    } catch (e) {
        return {
            data: JSON.parse(e.response.body),
            type: type
        };
    }
}

const apiRequest = async function(req, res, method) {
    if (req.session.hasOwnProperty('access_token') && req.session.access_token && req.params.hasOwnProperty('0')) {
        let valid = false;
        let response = '';
        try {
            let placebo = await got(`https://www.deviantart.com/api/v1/oauth2/placebo?access_token=${req.session.access_token}`);
            valid = JSON.parse(placebo.body).status === 'success';
        } catch (e) {
            valid = false;
        }
        if (!valid) {
            let status = 'client';
            if (req.session.type === 'user') {
                status = await token(req, 'refresh_token');
            }
            if (status !== 'success') {
                req.session.type = 'client';
            }
        }
        let status = await token(req, 'client_credentials');
        if (status !== 'success') {
            return res.json({error: 'INVALID_TOKEN'});
        }

        if (method === 'GET') {

            let matchRoute = accessRequired.hasOwnProperty(req.params[0]);
            let matchQuery = true;
            if (matchRoute && accessRequired[req.params[0]].hasOwnProperty('query')) {
                matchQuery = req.query.hasOwnProperty(accessRequired[req.params[0]].query[0]) && req.query[accessRequired[req.params[0]].query[0]] === accessRequired[req.params[0]].query[1];
            }
            response = await api(matchQuery && matchRoute ? req.session.access_token : req.session.client_token, req.params[0], req.session.type, new URLSearchParams(req.query)); 
        } else {
            response = await apiPost(req.session.access_token, req.params[0], req.session.type, req.body);
        }
        return res.json(response);
    } else {
        let status = await token(req, 'client_credentials');
        if (status === 'success') {
            if (method === 'GET') {
                response = await api(req.session.client_token, req.params[0], req.session.type, new URLSearchParams(req.query));
            } else {
                return res.json({error: 'UNAUTHORIZED_TOKEN'});
            }
            return res.json(response);
        } else {
            return res.json({error: 'INVALID_TOKEN'});
        }
    }
};

app
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .engine('ejs', ejs)
  .get('/', (req, res) => res.render('pages/index'))
  .get('/authorize', (req, res) => {
      req.session.state = uuid.v4();
      req.session.current_page = req.query.route;
      return res.redirect(`https://www.deviantart.com/oauth2/authorize?response_type=code&client_id=12312&redirect_uri=${callbackUrl(req.hostname, req.protocol)}&scope=${encodeURIComponent('basic comment.post user.manage collection gallery browse browse.mlt feed message')}&state=${req.session.state}`);
  })
  .get('/callback', async (req, res) => {
      let status = 'error';
      if (req.query.hasOwnProperty('code') && req.query.hasOwnProperty('state')) {
          if (req.session.state !== req.query.state) {
              return res.redirect(`/#!${req.session.current_page}?status=${status}`);
          }
          status = await token(req, 'authorization_code', req.query.code);
      }
      return res.redirect(`/#!${req.session.current_page}?status=${status}`);
  })
    .post('/revoke', async (req, res) => {
        try {
            let response = await got.post('https://www.deviantart.com/oauth2/revoke', {form: {token: req.session.access_token}});
            req.session.type = 'client';
            delete req.session.access_token;
            delete req.session.refresh_token;
            req.session.destroy();
            req.session = null;
            return res.json(JSON.parse(response.body));
        } catch(e) {
            return res.json(JSON.parse(e.response.body));
        }
    })
    .get('/api/*', async (req, res) => {
        apiRequest(req, res, 'GET');
    })
    .post('/api/*', async (req, res) => {
        apiRequest(req, res, 'POST');
    })
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
