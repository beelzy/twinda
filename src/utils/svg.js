var m = require('mithril');

var manifest = {
    arrow: {
        src: 'chevron-with-circle-right.svg'
    },
    about: {
        src: 'help-with-circle.svg'
    },
    deviantart: {
        src: 'deviantart.svg',
        size: [596, 369]
    },
    gitlab: {
        src: 'gitlab.svg',
        size: [340, 315]
    },
    login: {
        src: 'login.svg'
    },
    logout: {
        src: 'log-out.svg'
    },
    inbox: {
        src: 'inbox.svg'
    },
    angle: {
        src: 'chevron-right.svg'
    },
    fave: {
        src: 'star.svg'
    },
    unfave: {
        src: 'star-outlined.svg'
    },
    comment: {
        src: 'typing.svg'
    },
    search: {
        src: 'magnifying-glass.svg'
    },
    documents: {
        src: 'documents.svg'
    },
    sort: {
        src: 'funnel.svg'
    },
    topic: {
        src: 'open-book.svg'
    },
    info: {
        src: 'info-with-circle.svg'
    },
    link: {
        src: 'link.svg'
    },
    image: {
        src: 'image.svg'
    },
    facebook: {
        src: 'facebook.svg'
    },
    twitter: {
        src: 'twitter.svg'
    },
    tumblr: {
        src: 'tumblr.svg'
    },
    pinterest: {
        src: 'pinterest.svg'
    },
    share: {
        src: 'share.svg'
    },
    stats: {
        src: 'line-graph.svg'
    },
    download: {
        src: 'download.svg'
    },
    watch: {
        src: 'add-user.svg'
    },
    unwatch: {
        src: 'remove-user.svg'
    },
    folder: {
        src: 'folder.svg'
    },
    close: {
        src: 'circle-with-cross.svg'
    },
    users: {
        src: 'users.svg'
    },
    chat: {
        src: 'chat.svg'
    },
    calendar: {
        src: 'calendar.svg'
    },
    user: {
        src: 'user.svg'
    },
    images: {
        src: 'images.svg'
    },
    book: {
        src: 'book.svg'
    },
    clock: {
        src: 'stopwatch.svg'
    },
    slideshow: {
        src: 'video.svg'
    },
    home: {
        src: 'home.svg'
    },
    list: {
        src: 'text-document.svg'
    },
    logo: {
        src: 'logo.svg'
    },
    mail: {
        src: 'mail.svg'
    },
    flash: {
        src: 'flash.svg'
    },
    ribbon: {
        src: 'price-ribbon.svg'
    },
    heart: {
        src: 'heart.svg'
    },
    supergroup: {
        id: 'home',
        src: 'home.svg',
        cssClass: '.brand'
    }
};

module.exports = {
    loading: () => {
        return m.trust('<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="64px" height="64px" viewBox="0 0 128 128" xml:space="preserve"><g><path fill="currentColor" fill-opacity="1" d="M64,128a64,64,0,1,1,64-64A64,64,0,0,1,64,128ZM64,2.75A61.25,61.25,0,1,0,125.25,64,61.25,61.25,0,0,0,64,2.75Z"/><path fill="currentColor" fill-opacity="1" d="M64 128a64 64 0 1 1 64-64 64 64 0 0 1-64 64zM64 2.75A61.2 61.2 0 0 0 3.34 72.4c1.28-3.52 3.9-6.32 7.5-6.86 6.55-1 11.9 2.63 13.6 8.08 3.52 11.27.5 23 15 35.25 19.47 16.46 40.34 13.54 52.84 9.46A61.25 61.25 0 0 0 64 2.75z"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1800ms" repeatCount="indefinite"></animateTransform></g></svg>');
    },
    icon: (key) => {
        let icon = manifest[key];
        return module.exports.template(icon.src, icon.id ? icon.id : key, icon.size, icon.cssClass, icon.title, icon.position, icon.size2);
    },
    template: (src, id, size, cssClass, title, position, size2) => {
        size = size ? size : [20, 20];
        size2 = size2 ? size2 : size;
        position = position ? position : [0, 0];
        return m(`svg${cssClass ? cssClass : ''}[viewBox="${position[0]} ${position[1]} ${size[0]} ${size[1]}"]`,
            [
                title ? m('title', title) : null,
                m(`use[xlink:href=icons/${src}#${id}][height="${size2[1]}px"][width="${size2[0]}px"]`
                )
            ]
        );
    }
};
