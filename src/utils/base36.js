const Base36 = {
    decode: (encoded) => {
        return parseInt(encoded, 36);
    },
    encode: (decoded) => {
        return Number(decoded).toString(36);
    }
};

module.exports = Base36;
