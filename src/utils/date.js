const DateFormatter = {
    formatFromString: (dateString) => {
        let date = new Date(dateString);
        return DateFormatter.formatFromObject(date);
    },
    formatFromTimestamp: (timestamp) => {
        let date = new Date(parseInt(timestamp) * 1000);
        return DateFormatter.formatFromObject(date);
    },
    formatFromObject: (date) => {
        return `${date.toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric'})} ${date.toLocaleTimeString()}`;
    }
}

module.exports = DateFormatter;
