const LocalStorage = {
    init: () => {
        try {
            window.localStorage.setItem(LocalStorage.test, 'TEST');
            window.localStorage.removeItem(LocalStorage.test);
            LocalStorage.data = window.localStorage[LocalStorage.key] ? JSON.parse(window.localStorage[LocalStorage.key]) : {};
            LocalStorage.available = true;
        }
        catch(e) {
            console.log(e.code, e.name);
            LocalStorage.available = false;
        }
    },
    set: (key, value) => {
        if (LocalStorage.available) {
            LocalStorage.data[key] = value;
            window.localStorage.setItem(LocalStorage.key, JSON.stringify(LocalStorage.data));
        }
    },
    data: {},
    key: 'twinDA',
    test: 'twinDA-test',
    available: false
};

module.exports = LocalStorage;
