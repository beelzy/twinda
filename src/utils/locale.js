const Locale = {
    title: 'This Website Is Not DeviantArt',
    statusExpand: 'View Comments',
    journalExpand: 'Read More',
    commentsDisabled: '(Comments disabled)',
    nocomments: '(No comments)',
    noentries: '(No entries)',
    nofolders: '(No folders)',
    nodeviations: '(No deviations found.)',
    nodeviationsfolder: '(No deviations in this folder)',
    loadmore: 'Load More',
    rootFolder: 'Featured',
    replybutton: 'Reply',
    replyplaceholder: 'Reply?',
    watchbutton: 'Watch'
};

module.exports = Locale;
