const m = require('mithril');

const mimeType = function(xhr) {
    xhr.overrideMimeType('application/json');
};

const Request = {
    queues: {default: []},
    loggedIn: false,
    cleanupRequests: () => {
        let keys = Object.keys(Request.queues);
        for(let i = 0, size = keys.length; i < size; i++) {
            let queue = Request.queues[keys[i]];
            Request.queues[keys[i]] = queue.filter(
                (request) => request !== null && request.readyState !== 4,
            );
        }
    },
    abortRequests: (key) => {
        let queue = Request.queues[key];
        if (queue) {
            for(let i = 0, size = queue.length; i < size; i += 1) {
                let request = queue[i];
                if (request !== null) {
                    request.abort();
                    request = null;
                }
            }
            Request.queues[key] = [];
        }
    },
    stopAllRequests: () => {
        let keys = Object.keys(Request.queues);
        for(let i = 0, size = keys.length; i < size; i++) {
            Request.abortRequests(keys[i]);
        }
    },
    get: (url, queue='default') => {
        return m.request({
            method: 'GET',
            url: url,
            config: (xhr) => {
                mimeType(xhr);
                Request.queues[queue] = Request.queues[queue] || [];
                Request.queues[queue].push(xhr);
            }
        }).then((response) => {
            if (response.hasOwnProperty('error') && response.error === 'INVALID_TOKEN') {
                window.location = `/authorize?route=${m.route.get()}`;
            }
            return Promise.resolve(response);
        }, (e) => {
            return Promise.reject(e);
        }).finally(() => {
            Request.cleanupRequests();
        });
    },
    post: (url, data={}, queue='default') => {
        return m.request({
            method: 'POST',
            url: url,
            config: mimeType,
            body: data
        }).then((response) => {
            if (response.hasOwnProperty('error') && response.error === 'INVALID_TOKEN') {
                window.location = `/authorize?route=${m.route.get()}`;
            }
            return Promise.resolve(response);
        }, (e) => {
            return Promise.reject(e);
        }).finally(() => {
            Request.cleanupRequests();
        });
    },
    apiGet: (url, queue='default') => {
        return Request.get(`/api/${url}`, queue).then((response) => {
            Request.loggedIn = response.type === 'user';
            return Promise.resolve(response);
        });
    },
    apiPost: (url, data={}, queue='default') => {
        return Request.post(`/api/${url}`, data, queue);
    },
    browse: (slug, queue='default') => {
        return Request.apiGet(`browse/${slug}`, queue);
    },
    feed: (slug, queue='default') => {
        return Request.apiGet(`feed/${slug}`, queue);
    },
    deviation: (slug, queue='default') => {
        return Request.apiGet(`deviation/${slug}`, queue);
    },
    comments: (slug, queue='default') => {
        return Request.apiGet(`comments/${slug}`, queue);
    },
    user: (slug, queue='default') => {
        return Request.apiGet(`user/${slug}`, queue);
    },
    collections: (slug, queue='default') => {
        return Request.apiGet(`collections/${slug}`, queue);
    },
    gallery: (slug, queue='default') => {
        return Request.apiGet(`gallery/${slug}`, queue);
    },
    commentsPost: (slug, data, queue='default') => {
        return Request.apiPost(`comments/${slug}`, data, queue);
    },
    collectionsPost: (slug, data, queue='default') => {
        return Request.apiPost(`collections/${slug}`, data, queue);
    },
    userPost: (slug, data, queue='default') => {
        return Request.apiPost(`user/${slug}`, data, queue);
    }
};

module.exports = Request;
