const Parser = {
    externalRegex: /^http(s)*:\/\/(www.)*deviantart.com\/users\/outgoing\?/,
    stripScripts: (html) => {
        let div = document.createElement('div');
        div.innerHTML = html;
        let scripts = div.getElementsByTagName('script');
        let i = scripts.length;
        while (i--) {
            scripts[i].parentNode.removeChild(scripts[i]);
        }
        return div.innerHTML;
    },
    nocookieYoutube: (html) => {
        let element = document.createElement('div');
        element.innerHTML = html;
        let iframes = element.querySelectorAll('iframe');
        for (let i = 0, size = iframes.length; i < size; i++) {
            iframes[i].src = iframes[i].src.replace(/^http(s)*:\/\/(www.)*youtube.com/, 'https://www.youtube-nocookie.com');
        }
        return element.innerHTML;
    },
    stripOutgoing: (html) => {
        let element = document.createElement('div');
        element.innerHTML = html;
        let links = element.querySelectorAll('a');
        for (let i = 0, size = links.length; i < size; i++) {
            let link = links[i];
            if (link.href.match(Parser.externalRegex)) {
                link.target = '_blank';
                link.classList.add('external');
            }
            link.href = link.href.replace(Parser.externalRegex, '');
        }
        return element.innerHTML;
    },
    parseAll: (html) => {
        html = Parser.stripScripts(html);
        html = Parser.nocookieYoutube(html);
        html = Parser.stripOutgoing(html);

        return html;
    }
};

module.exports = Parser;
