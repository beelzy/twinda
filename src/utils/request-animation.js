const RequestAnimation = {
    requestTimeout: function(fn, delay) {
        let start = new Date().getTime();
        let handle = new Object();
        let loop = function() {
            let current = new Date().getTime();
            let delta = current - start;
            delta >= delay ? fn.call() : handle.value = window.requestAnimationFrame(loop);
        };

        handle.value = window.requestAnimationFrame(loop);
        return handle;
    }
};

module.exports = RequestAnimation;
