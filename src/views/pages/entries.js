const m = require('mithril');

const DateFormatter = require('../../utils/date.js');
const Tabs = require('../components/tabs.js');
const Pagination = require('../components/pagination.js');
const Entry = require('../components/entry.js');
const Journal = require('../../models/journal.js');
const Statuses = require('../../models/statuses.js');
const SVG = require('../../utils/svg.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    oninit: (vnode) => {
        let model = vnode.attrs.model === 'journal' ? Journal : Statuses;
        model.init(vnode.attrs.user);

        model.fetch();
    },
    view: (vnode) => {
        let isJournal = vnode.attrs.model === 'journal';
        let model = isJournal ? Journal : Statuses;
        let loading = m('.Loading.Loading__Overlay.Loading__Overlay--Left', SVG.loading());
        return m('.Wrapper.Wrapper__Tabbed.Wrapper__With-Header.Entries', [
            m(Tabs),
            m('.Profile__Bar', isJournal ? m('.Buttons', [
                m(`button${model.featured ? '' : '.active'}`, {
                    disabled: !model.featured,
                    onclick: () => {
                        model.toggleFeatured(false);
                    }
                }, 'All'),
                m(`button${model.featured ? '.active' : ''}`, {
                    disabled: model.featured, onclick: () => {
                        model.toggleFeatured(true);
                    }
                }, 'Featured')
            ]) : m('h1.title', 'Statuses')),
            m('.Content.Content__Tabbed', [
                m(`.Page.Page__Tabbed.Page__With-Sidebar.Entries__Main${model.selectedMenuItem === 'pagination' ? '.with-pagination' : ''}`,
                    model.data.hasOwnProperty('results') ? m('.Entries__Main--List', [
                        model.loading ? loading : (model.data.results.length > 0 ? model.data.results.map((entry) => {
                            return m(Entry, {model: vnode.attrs.model, entry: entry});
                        }) : m('.noentries', Locale.noentries)),
                        m(Pagination, {
                            selector: `.Pagination__Fixed.Pagination__Fixed--Left${model.selectedMenuItem === 'pagination' ? '' : '.hidden-sm'}`,
                            index: model.page.index,
                            total: model.page.total,
                            mobilePages: model.page.cacheMobile,
                            pages: model.page.cache,
                            changePageFn: model.changePage
                        })
                    ]) : (model.data.hasOwnProperty('error') ? m('span', model.data.error_description) : loading)
                ),
                m('.Entries__Sidebar.Menu__Fluid.Menu', m('.Menu__Contents', [
                    m(`h3.Icon.Menu__Option.hidden-md${model.selectedMenuItem === 'list' ? '.selected' : ''}`, {
                        onclick: () => {
                            model.selectMenuItem('list');
                        }
                    }, [SVG.icon('list'), m('div', 'List')]),
                    m(`ul.List.List__Title${model.selectedMenuItem === 'list' ? '' : '.hidden-sm'}`,
                        model.data.hasOwnProperty('results') && !model.loading
                        ? model.data.results.map((entry) => {
                            let title = isJournal ? entry.title : DateFormatter.formatFromString(entry.ts);
                            return m('li', m(m.route.Link, {
                                href: isJournal ? `/deviation/${entry.deviationid}` : `/status/${entry.statusid}`,
                                title: title
                            }, [
                                m('h4', title),
                                isJournal
                                ? m('span', DateFormatter.formatFromTimestamp(entry.published_time)) : ''
                            ]));
                        }) : (model.data.hasOwnProperty('error')
                            ? m('li', model.data.error_description) : m('li.Loading', SVG.loading()))
                    ),
                    m(`h3.Icon.Menu__Option.hidden-md${model.selectedMenuItem === 'pagination' ? '.selected' : ''}`, {
                        onclick: () => {
                            model.selectMenuItem('pagination');
                        }
                    }, [SVG.icon('documents'), m('div', 'Page')])
                ]))
            ])
        ]);
    }
};
