const m = require('mithril');

const Gallery = require('../../models/gallery.js');
const Collections = require('../../models/collections.js');
const User = require('../../models/user.js');
const Tabs = require('../components/tabs.js');
const Thumbnail = require('../components/thumbnail.js');
const Pagination = require('../components/pagination.js');
const Model = require('../../models/gallery-model.js');
const SVG = require('../../utils/svg.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    oninit: (vnode) => {
        let model = vnode.attrs.model === 'gallery' ? Gallery : Collections;
        let oldId = model.id;
        let oldUser = model.username;
        if (model.id !== vnode.attrs.folderid || model.username !== vnode.attrs.user || model.mature !== User.mature) {
            model.init(vnode.attrs.folderid, vnode.attrs.user, vnode.attrs.model === Model.model);

            if (vnode.attrs.model !== 'gallery') {
                model.folders.normalizeAllFolder();
                model.id = model.folders.folderid;
            }

            let preloadGallery = vnode.attrs.model === 'gallery' || vnode.attrs.folderid !== 'all';
            if (preloadGallery) {
                model.fetchGallery();
            }

            if (oldUser !== vnode.attrs.user || Model.model !== vnode.attrs.model || model.mature !== User.mature) {
                if (vnode.attrs.model !== 'gallery' && model.folders.folderid === 'all') {
                    model.loading = true;
                }
                model.fetchFolders().then((response) => {
                    if (vnode.attrs.model !== 'gallery'
                        && !model.folders.error) {
                        if (model.id === 'all') {
                            model.folders.normalizeAllFolder();
                        }
                        model.id = model.folders.folderid;
                        model.fetchGallery();
                    }
                    if (model.folders.error) {
                        model.loading = false;
                    }
                });
            }
        }
        model.mature = User.mature;
        Model.model = vnode.attrs.model;
    },
    view: (vnode) => {
        let model = vnode.attrs.model === 'gallery' ? Gallery : Collections;
        let backlink = `/${vnode.attrs.model}/${model.username}/${model.folders.parentid}`;
        return m('.Wrapper.Wrapper__With-Header.Wrapper__Tabbed.Gallery', [
            m(Tabs),
            m('.Profile__Bar.Profile__Bar--Title', [
                m('h1.title', model.folders.folderid === 'all'
                    ? 'All' : (model.folders.name !== '' ? model.folders.name : '')),
                m('.Buttons', [
                    model.folders.folderid === 'all' && vnode.attrs.model === 'gallery' ? [
                        m(`button.Icon.Icon__Tab${model.isPopular ? '' : '.active'}`, {
                            disabled: !model.isPopular,
                            onclick: () => {
                                model.togglePopular(false);
                            }
                        }, [SVG.icon('clock'), m('span.hidden-sm', 'New')]),
                        m(`button.Icon.Icon__Tab${model.isPopular ? '.active' : ''}`, {
                            disabled: model.isPopular,
                            onclick: () => {
                                model.togglePopular(true);
                            }
                        }, [SVG.icon('fave'), m('span.hidden-sm', 'Popular')])
                    ] : '',
                    m('button.Icon.Icon__Tab', [SVG.icon('slideshow'), m('span.hidden-sm', 'Slideshow')])
                ])
            ]),
            m('.Content.Content__Tabbed', [
                m('.Gallery__Menu.Menu', m('.Menu__Contents', [
                    model.folders.parentid && model.folders.parentid !== ''
                    ? m(m.route.Link, {
                        class: 'Menu__Option Menu__Option--Back hidden-md',
                        href: backlink
                    }, '<') : '',
                    m(`h3.Icon.Menu__Option.hidden-md${model.selectedMenuItem === 'default' ? '.selected' : ''}`,
                        {onclick: () => {
                            model.selectMenuItem('default');
                        }}, [SVG.icon('home'), m('div', 'Main Folders')]),
                    m(`ul.Folders.List.List__Sidebar${model.selectedMenuItem === 'default' ? '' : '.hidden-sm'}`, [
                        vnode.attrs.model === 'gallery'
                        ? m(`li${model.id === 'all' ? '.selected' : ''}`,
                            model.id === 'all' ? m('span', 'All') : m(m.route.Link, {
                                href: `/${vnode.attrs.model}/${model.username}`
                            }, 'All')) : '',
                        model.folders.rootid === ''
                        ? (model.folders.error
                            ? m('li', model.folders.error_description) : m('li.Loading', SVG.loading())
                        ) : m(`li${model.id === model.folders.rootid ? '.selected' : ''}`,
                            model.id === model.folders.rootid ? m('span', Locale.rootFolder)
                            : m(m.route.Link, {
                                href: `/${vnode.attrs.model}/${model.username}/${model.folders.rootid}`
                            }, Locale.rootFolder)),
                        model.folders.parentid && model.folders.parentid !== ''
                        ? m('li', m(m.route.Link, {class: '.hidden-sm', href: backlink}, 'Back')) : ''
                    ]),
                    m(`h3.Icon.Menu__Option${model.selectedMenuItem === 'folders' ? '.selected' : ''}`, {onclick: () => {
                        model.selectMenuItem('folders');
                    }}, [
                        SVG.icon('folder'),
                        m('div', `${vnode.attrs.model === 'gallery' ? 'Gallery' : 'Collections'} Folders`)
                    ]),
                    m(`ul.Folders.Folders__Thumbnails.List${model.selectedMenuItem === 'folders'
                            ? '' : '.hidden-sm'}`, [
                        model.folders.display.length > 0 ? model.folders.display.map((folder) => {
                            let url = `/${vnode.attrs.model}/${model.username}/${folder.folderid}`;
                            return m('li', [
                                m(Thumbnail, {
                                    selector: '.hidden-sm',
                                    deviation: folder.hasOwnProperty('deviations')
                                    && folder.deviations.length > 0 ? folder.deviations[0] : {},
                                    nouser: true,
                                    notitle: true,
                                    nostats: true,
                                    link: url
                                }),
                                m(m.route.Link, {href: url}, folder.name)
                            ]);
                        }) : m('li', Locale.nofolders),
                        model.folders.loading ? m('li.Loading', SVG.loading())
                                : (model.folders.error ? m('li', model.folders.error_description) : '')
                    ]),
                    m(`h3.Menu__Option.Icon.hidden-md${model.selectedMenuItem === 'pagination' ? '.selected' : ''}`, {
                        onclick: () => {
                            model.selectMenuItem('pagination');
                        }
                    }, [SVG.icon('documents'), m('div', 'Page')])
                ])),
                m(`${model.selectedMenuItem === 'pagination'
                        ? '.with-pagination' : ''}.Page.Page__Tabbed.Grid.Grid__Tabbed`, [
                    model.data.hasOwnProperty('results') ? [
                        model.data.results.length > 0 ? model.data.results.map((item) => {
                            return m(Thumbnail, {deviation: item});
                        }) : m('span.empty', Locale.nodeviationsfolder),
                        m(Pagination, {
                            selector: `${model.selectedMenuItem === 'pagination' ? '' : '.hidden-sm'}.Pagination__Fixed.Pagination__Fixed--Right`,
                            index: model.page.index,
                            total: model.page.total,
                            mobilePages: model.page.cacheMobile,
                            pages: model.page.cache,
                            changePageFn: model.changePage
                        })
                    ] : '',
                    model.loading? m('.Loading.Loading__Overlay', SVG.loading()) : ''
                ])
            ])
        ]);
    }
};
