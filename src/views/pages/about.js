const m = require('mithril');

module.exports = {
    view: (vnode) => {
        return m('.About.Page.Wrapper__With-Header', [
            m('h1', 'This Website Is Not DeviantArt'),
            m('p', [`This Website Is Not DeviantArt (twinDA) is a prototype based loosely off of the old DeviantArt, using the `, m('a', {href: 'https://www.deviantart.com/developers/', target: '_blank'}, 'DeviantArt API'), `. Since people have been complaining about how unusable the new Eclipse DeviantArt layout is, I've gathered people's most common complaints regarding the frontend and used them to help redesign it to be more usable.`]),
            m('h2', 'Features'),
            m('ul', [
                m('li', 'Simple and clean look and feel'),
                m('li', 'Pagination in browse, status and gallery pages'),
                m('li', 'No giant, clunky elements'),
                m('li', [`Light and fast frontend. It's made with `, m('a.external', {href: 'https://mithril.js.org', target: '_blank'}, 'Mithril'), `, a strong but light single page app library.`]),
                m('li', 'Responsive layout that works on desktop as well as mobile'),
                m('li', 'No ads'),
                m('li', 'Privacy-respecting share buttons (no extra 3rd-party scripts are loaded from Facebook, Twitter and co.)'),
                m('li', 'YouTube links are replaced with the GDPR compliant* version, youtube-nocookie.com'),
                m('li', 'Outgoing link redirects are removed; instead, the link opens in a new tab and a link icon appears next to the link.'),
                m('li', ['Free software, and decentralized. You can simply clone the code base, put it on a Node server somewhere with your own ', m('a', {href: 'https://www.deviantart.com/developers/register', target: '_blank'}, 'DeviantArt API keys'), ' and run it however you want.']),
                m('li', 'Strips the annoying outgoing link page and replaces it with a link that opens in a new tab when you click on an external link.')
            ]),
            m('.text-sm', `*This doesn't actually mean there are no cookies coming from YouTube, just that they are not used for tracking. Still better than vanilla YouTube links though.`),
            m('h2', 'What this prototype is not for:'),
            m('ul', [
                m('li', 'Managing groups'),
                m('li', 'Polls'),
                m('li', 'Forums'),
                m('li', 'DA chat'),
                m('li', `Regular usage. Unfortunately, the API is rate limited, which means you can't be browsing on it and posting much while logged in.`),
                m('li', [`A complete replacement of DeviantArt. While I do attempt to do things better than Eclipse with this prototype, twinDA can't do everything, and the server is not setup to be able to handle large loads of users. However, if you did want to have a server set up for this, you're free to take the code and put it on such a server, bearing in mind that you will still be limited by the API rate limits. If you're looking for another alternative, I suggest giving `, m('a.external', {href: 'https://paperdemon.com', target: '_blank'}, 'Paperdemon'), ` a try.`])
            ]),
            m('h2', 'FAQ'),
            m('h3', 'Why are you doing this?'),
            m('p', [`I'm not trying to replace DeviantArt. I want to show people that it is possible to create a website that is both fast, clean and privacy respecting. And it doesn't have to take forever to build either. Take a look at `, m('a.external', {href: 'https://whatdoesmysitecost.com', target: '_blank'}, 'whatdoesmysitecost.com'), ` and you will see browsing this website costs your mobile provider much less than browsing DeviantArt directly does.`]),
            m('p', `I'm doing this For fun. When I found out DeviantArt had an API, I wondered what a site based off of user complaints and suggestions could look like. And I wanted to know how long it would take me to make a frontend for a site like this from scratch.`),
            m('p', 'Because many users have complained that DeviantArt Eclipse is unusable. I wanted to know if their complaints were valid by developing this website using the complaints and suggestions as a guideline--and to see if this is really what users want.'),
            m('h3', 'Who are you?'),
            m('p', `I do not work for DeviantArt or Wix. I am not paid to make this prototype. I am just a regular member of DeviantArt who has simply just been away from that site for too long. I came back and found plenty of people complaining about Eclipse, and they are leaving and now you can't even choose to use the old site layout anymore. I develop websites at work, and I realized this is something I could actually do on my spare time.`),
            m('h3', `Something doesn't work or a feature is missing.`),
            m('p', [`Unfortunately, there are things the DeviantArt API does not currently do. I have no idea `, m('a.external', {href: 'https://github.com/wix-incubator/DeviantArt-API/issues/88', target: '_blank'}, 'when'), ` or `, m('a.external', {href: 'https://github.com/wix-incubator/DeviantArt-API/issues/122', target: '_blank'}, 'if'), ` they plan to implement some of the features, but I'm not `, m('a.external', {href: 'https://github.com/wix-incubator/DeviantArt-API/issues/153', target: '_blank'}, 'holding my breath'), ` over it.`]),
            m('p', ['If there is an issue with the way this site functions that has nothing to do with the API, feel free to file an ', m('a.external', {href: 'https://gitlab.com/beelzy/twinda/-/issues', target: '_blank'}, 'issue'), ' on Gitlab.']),
            m('h3', 'The site sometimes loads slowly.'),
            m('p', `This does sometimes happen when the API traffic is higher. Unfortunately, there's not much I can do about it.`),
            m('h3', 'Why does the home page show the newest deviations by default instead of the popular ones?'),
            m('p', `Because the popular page doesn't give you an accurate reflection of what the great majority of work that gets submitted to DeviantArt is like; only what they want people to see. If this bothers you, you can still use the popular filter.`),
            m('h2', 'Roadmap'),
            m('ul', [
                m('li', 'Slideshow Feature'),
                m('li', 'Gallery and Collections Folder Management'),
                m('li', 'Profile Editing'),
                m('li', 'More advanced comments/literature editor'),
                m('li', 'Notes Management'),
                m('li', 'Managing Sta.sh submissions')
            ]),
            m('h2', 'Credits/Attribution'),
            m('ul', [
                m('li', ['Loading icon from ', m('a.external', {href: 'https://icons8.com/preloaders/', target: '_blank'}, 'Preloaders.net')]),
                m('li', [m('a.external', {href: 'http://www.entypo.com', target: '_blank'}, 'Entypo+'), ' icons by Daniel Bruce, CC BY-SA 4.0'])
            ]),
			m('h2', 'Data Collection and Usage'),
            m('p', `twinDA does not track you or display ads or any 3rd party content other than whatever comes from the DeviantArt API. We try to replace YouTube links with the GDPR compliant version, youtube-nocookie.com, but this does not guarantee that any other iframe content that's allowed by DeviantArt will not attempt to load malicious or tracking 3rd-party scripts. We also strip script tags from deviation contents such as literature entries and journals. The only things twinDA retains are the session cookies which are used to keep track of access tokens so that you can post comments and interact with DeviantArt with an account and a local storage value to keep track of the mature filter preference.`)
        ]);
    }
};
