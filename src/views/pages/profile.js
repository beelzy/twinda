const m = require('mithril');

const DateFormatter = require('../../utils/date.js');
const Profile = require('../../models/profile.js');
const User = require('../../models/user.js');
const Thumbnail = require('../components/thumbnail.js');
const Card = require('../components/card.js');
const Comments = require('../components/comments.js');
const Tabs = require('../components/tabs.js');
const Reply = require('../components/reply.js');
const SVG = require('../../utils/svg.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    oninit: (vnode) => {
        Profile.init(vnode.attrs.id);
        Profile.fetchStatuses();
        Profile.fetchWatching();
        Profile.fetchWatchers();
        Profile.fetchComments();
    },
    view: (vnode) => {
        return m('.Wrapper.Wrapper__Tabbed.Wrapper__With-Header.Profile', [
            m(Tabs),
            m('.Profile__Bar', m('.Profile__Bar--ScrollWrapper', Profile.data.hasOwnProperty('stats') ? [
                m('.Profile__Bar--Column', [
                    Profile.data.country ? m('.Info__Item', [
                        m('h4', 'Country'),
                        m('span', Profile.data.country)
                    ]) : '',
                    Profile.data.website ? m('.Info__Item', [
                        m('h4', 'Website'),
                        m('span', m('a', {
                            target: '_blank',
                            href: Profile.data.website
                        }, Profile.data.website))
                    ]) : ''
                ]),
                m('.Profile__Bar--Column', [
                    m('.Info__Item', [
                        m('h4', 'Deviations'),
                        m('span', Profile.data.stats.user_deviations)
                    ]),
                    m('.Info__Item', [
                        m('h4', 'Collections'),
                        m('span', Profile.data.stats.user_favourites)
                    ]),
                    m('.Info__Item', [
                        m('h4', 'Comments Made'),
                        m('span', Profile.data.stats.user_comments)
                    ]),
                    m('.Info__Item', [
                        m('h4', 'Pageviews'),
                        m('span', Profile.data.stats.profile_pageviews)
                    ]),
                    m('.Info__Item', [
                        m('h4', 'Comments Received'),
                        m('span', Profile.data.stats.profile_comments)
                    ])
                ])
            ] : (Profile.data.hasOwnProperty('error') ? m('span', Profile.data.error_description)
                : m('.Loading', SVG.loading())))),
            m('.Content.Content__Column', [
                m('.Profile__Column', [
                    m(Card, {
                        header: 'Featured',
                        icon: 'image',
                        open: true,
                        content: Profile.hasFeatured ? (Profile.featured.hasOwnProperty('deviationid') ? [
                            m(Thumbnail, {size: 'Large', deviation: Profile.featured}),
                            m(m.route.Link, {href: `/gallery/${Profile.username}/${Profile.featuredid}`},
                                m('button', 'Browse Folder'))
                        ] : m('.Loading', SVG.loading())) : m('.nofeatured', '(No featured artwork)')
                    }),
                    m(Card, {
                        header: 'Collections',
                        icon: 'folder',
                        open: true,
                        content: Profile.collection.hasOwnProperty('results')
                        ? (Profile.collection.results.length > 0 ? [
                            m('ul.collections', Profile.collection.results.map((collection) => {
                                return m('li', m(Thumbnail, {size: 'Small', deviation: collection}));
                            })),
                            m(m.route.Link, {href: `/collections/${Profile.username}`},
                                m('button', 'Browse Collections'))
                        ] : m('.nocollections', '(No featured collections)'))
                        : (Profile.collection.hasOwnProperty('error')
                            ? m('span', Profile.collection.error_description)
                            : m('.Loading', SVG.loading()))
                    }),
                    m(Card, {
                        header: 'Watching',
                        icon: 'users',
                        open: true,
                        content: Profile.watching.error ? m('span', Profile.watching.error_description)
                        : (!Profile.watching.hasMore && Profile.watching.data.length === 0
                            ? m('.nowatching', '(Not watching anybody)') : [
                                Profile.watching.data.length > 0 ?
                                m('ul.List.List__User.List__User--Grid', Profile.watching.data.map((watch) => {
                                    return m('li',
                                        m(m.route.Link,
                                            {href: `/profile/${watch.user.username}`},
                                            [
                                                m('img.Avatar', {src: watch.user.usericon}),
                                                m(`.title.Icon__Button${User.bannedClass(watch.user.type)}`, [watch.user.username, User.getRankIcon(watch.user.type)])
                                            ]
                                        )
                                    );
                                })) : '',
                                Profile.watching.loading ? m('.Loading', SVG.loading()) : '',
                                Profile.watching.hasMore && !Profile.watching.error
                                ? m('button.loadmore', {
                                    onclick: Profile.fetchWatching,
                                    disabled: Profile.watching.loading
                                }, Locale.loadmore) : ''
                            ])
                    }),
                    m(Card, {
                        header: 'Watchers',
                        icon: 'users',
                        open: true,
                        content: Profile.watchers.error ? m('span', Profile.watchers.error_description)
                        : (!Profile.watchers.hasMore && Profile.watchers.data.length === 0
                            ? m('.nowatchers', '(No watchers)') : [
                                Profile.watchers.data.length > 0 ?
                                m('ul.List.List__User.List__User--Grid', Profile.watchers.data.map((watch) => {
                                    return m('li',
                                        m(m.route.Link,
                                            {href: `/profile/${watch.user.username}`},
                                            [
                                                m('img.Avatar', {src: watch.user.usericon}),
                                                m(`.title.Icon__Button${User.bannedClass(watch.user.type)}`, [watch.user.username, User.getRankIcon(watch.user.type)])
                                            ]
                                        )
                                    );
                                })) : '',
                                Profile.watchers.loading ? m('.Loading', SVG.loading()) : '',
                                Profile.watchers.hasMore && !Profile.watchers.error
                                ? m('button.loadmore', {
                                    onclick: Profile.fetchWatchers,
                                    disabled: Profile.watchers.loading
                                }, Locale.loadmore) : ''
                            ])
                    }),
                ]),
                m('.Profile__Column', [
                    m(Card, {
                        header: 'About',
                        icon: 'info',
                        open: true,
                        content: Profile.data.hasOwnProperty('bio') ? [
                            Profile.data.profile_pic ? m('img', {src: Profile.data.profile_pic.preview.src}) : '',
                            m('.bio', m.trust(Profile.data.bio))
                        ] : (Profile.data.hasOwnProperty('error')
                            ? m('span', Profile.data.error_description) : m('.Loading', SVG.loading()))
                    }),
                    m(Card, {
                        header: 'Statuses',
                        icon: 'calendar',
                        open: true,
                        content: Profile.statuses.hasOwnProperty('results')
                        ? (Profile.statuses.results.length > 0
                            ? [
                                m('.List__Statuses', Profile.statuses.results.map((status) => {
                                    let link = `/status/${status.statusid}`;
                                    return m('.Entry', [
                                        m('.Entry__Title', m(m.route.Link,
                                            {href: link},
                                            DateFormatter.formatFromString(status.ts)
                                        )),
                                        m('hr'),
                                        m('.Entry__Content', m.trust(status.body)),
                                        m('hr'),
                                        m('.Entry__Footer', [
                                            m('.Entry__Footer--Stats.Icon__Thumbnail', [
                                                SVG.icon('comment'),
                                                m('span', status.comments_count)
                                            ]),
                                            m(m.route.Link, {class: 'Entry__Footer--Button', href: link},
                                                m('button', Locale.statusExpand))
                                        ])
                                    ]);
                                })),
                                m(m.route.Link, {href: `/statuses/${Profile.username}`}, m('button', 'Browse Statuses'))
                            ] : m('.nostatuses', '(No statuses)'))
                        : (Profile.statuses.hasOwnProperty('error')
                            ? m('span', Profile.statuses.error_description) : m('.Loading', SVG.loading()))
                    }),
                    m(Card, {
                        header: 'Comments',
                        icon: 'chat',
                        open: true,
                        content: [
                            Profile.comments.main.hasOwnProperty('error') ? '' : m(Reply, {
                                loading: Profile.postingComment,
                                postCommentFn: Profile.postComment,
                                setCommentFn: Profile.setCommentEdit,
                                commentEdit: Profile.commentEdit
                            }),
                            Profile.comments.main.hasOwnProperty('thread')
                            ? (Profile.comments.main.thread.length > 0 ? [
                                m(Comments, {
                                    key: `${Profile.page.index}${Profile.postedCommentId}`,
                                    thread: Profile.comments.main.thread,
                                    page: Profile.page,
                                    changePageFn: Profile.changePage,
                                    replies: Profile.comments.replies,
                                    postCommentFn: Profile.postComment,
                                    loading: Profile.comments.loading,
                                    fixedPagination: true,
                                    replyEnabled: true
                                })
                            ] : m('span.nocomments', Locale.nocomments))
                            : (Profile.comments.main.hasOwnProperty('error')
                                ? m('span', Profile.comments.main.error_description)
                                : m('.Loading', SVG.loading()))
                        ]
                    })
                ])
            ])
        ]);
    }
};
