const m = require('mithril');

const Browse = require('../../models/browse.js');
const User = require('../../models/user.js');

const Pagination = require('../components/pagination.js');
const Thumbnail = require('../components/thumbnail.js');
const SVG = require('../../utils/svg.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    oninit: function(vnode) {
        if (Browse.mature !== User.mature) {
            Browse.topicIndex = 0;
            Browse.topics = [];
            Browse.moreTopics = true;
        }
        if (Browse.topicIndex === 0 && Browse.moreTopics) {
            Browse.fetchTopics();
        }
        Browse.init(vnode.attrs.id);
    },
    view: function(vnode) {
        let searchSelected = Browse.selectedMenuItem === 'search';
        let sortSelected = Browse.selectedMenuItem === 'sort';
        let topicsSelected = Browse.selectedMenuItem === 'topics';
        let isPagination = Browse.selectedMenuItem === 'pagination';
        return m('.Wrapper.Wrapper__With-Header.Browse', [
            m('.Menu', m('.Menu__Contents', [
                Browse.key === 'popular' || Browse.key === 'newest' ? [
                    m('.Menu__Search.Menu__Option', [
                        m(`h3.Menu__Option.Icon.Icon__Search${searchSelected ? '.selected' : ''}`,
                            {onclick: () => {
                                Browse.selectMenuItem('search');
                            }}, [SVG.icon('search'), m('.hidden-md', 'Search')]),
                        m(`input.List${searchSelected ? '' : '.hidden-sm'}`, {
                            type: 'text',
                            value: Browse.search,
                            oninput: (e) => {
                                Browse.search = e.currentTarget.value;
                                Browse.page.reset();
                                Browse.fetchPage();
                            },
                            placeholder: 'Search'
                        })
                    ])
                ] : '',
                m(`h3.Menu__Option.Icon${sortSelected ? '.selected' : ''}`,
                    {onclick: () => {
                        Browse.selectMenuItem('sort');
                    }}, [SVG.icon('sort'), m('div', 'Sort')]),
                m(`ul.List.List__Sidebar.sort${sortSelected ? '' : '.hidden-sm'}`,
                    {onclick: () => {
                        Browse.selectedMenuItem = '';
                    }}, [
                        m('li', m(m.route.Link,
                            {class: Browse.key === 'newest' ? 'selected' : '', href: '/browse/newest'},
                            'Newest')),
                        m('li', m(m.route.Link,
                            {class: Browse.key === 'popular' ? 'selected' : '', href: '/browse/popular'},
                            'Popular 24 hours')),
                        m('li', m(m.route.Link,
                            {class: Browse.key === 'hot' ? 'selected': '', href: '/browse/hot'},
                            'What\'s Hot')),
                        m('li', m(m.route.Link,
                            {class: Browse.key === 'undiscovered' ? 'selected' : '', href: '/browse/undiscovered'},
                            'Undiscovered')),
                        m('li', m(m.route.Link,
                            {class: Browse.key === 'dailydeviations' ? 'selected' : '', href: '/browse/dailydeviations'},
                            'Daily Deviations'))
                    ]),
                m(`h3.Menu__Option.Icon${topicsSelected ? '.selected' : ''}`, {
                    onclick: () => {
                        Browse.selectMenuItem('topics');
                    }}, [SVG.icon('topic'), m('div', 'Topics')]),
                m(`ul.List.List__Sidebar.topics${topicsSelected ? '' : '.hidden-sm'}`,
                    [
                        Browse.topics.length > 0 ? Browse.topics.map((topic) => {
                            let url = `/browse/topic/${topic.canonical_name}`;
                            return m('li', m(m.route.Link, {
                                onclick: () => {
                                    Browse.selectedMenuItem = '';
                                },
                                class: Browse.key === `topic?topic=${topic.canonical_name}` ? 'selected' : '', href: url
                            }, topic.name));
                        }) : '',
                        Browse.moreTopics ? m('li', m(`a.button.selected${Browse.loadingTopics ? '.Loading' : ''}`, {
                            onclick: () => {
                                if (!Browse.loadingTopics) {
                                    Browse.fetchTopics();
                                }
                            }
                        }, Browse.loadingTopics ? SVG.loading() : 'More Topics')) : ''
                    ]),
                m(`h3.Menu__Option.Icon.pagination-toggle${isPagination ? '.selected' : ''}`,
                    {onclick: () => {
                        Browse.selectMenuItem('pagination');
                    }}, [SVG.icon('documents'), m('div', 'Page')])
            ])),
            m(`.Page.Grid.${Browse.key}${isPagination ? '.with-pagination' : ''}`, [
                Browse.data.hasOwnProperty('results') ? [
                    Browse.data.results.length > 0 ? Browse.data.results.map((item) => {
                        return m(Thumbnail, {deviation: item});
                    }) : m('span.empty', Locale.nodeviations),
                    m(Pagination, {
                        selector: `.Pagination__Fixed.Pagination__Fixed--Right${isPagination ? '' : '.hidden-sm'}`,
                        index: Browse.page.index,
                        total: Browse.page.total,
                        mobilePages: Browse.page.cacheMobile,
                        pages: Browse.page.cache,
                        changePageFn: Browse.changePage})
                ] : '',
                Browse.loading ? m('.Loading.Loading__Overlay', SVG.loading()) : ''
            ])
        ]);
    }
};
