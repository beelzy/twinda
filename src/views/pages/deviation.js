const m = require('mithril');

const DateFormatter = require('../../utils/date.js');
const Base36 = require('../../utils/base36.js');
const Deviation = require('../../models/deviation.js');
const Collections = require('../../models/user-collections.js');
const User = require('../../models/user.js');
const Comments = require('../components/comments.js');
const Copy = require('../components/copy.js');
const Accordion = require('../components/accordion.js');
const Related = require('../components/related.js');
const Reply = require('../components/reply.js');
const SVG = require('../../utils/svg.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    oninit: (vnode) => {
        Deviation.data = {};
        Deviation.init(vnode.attrs.id).then(() => {
            Deviation.fetchComments();
        });
        Deviation.fetchRelated();
    },
    view: (vnode) => {
        let related = {
            artist: Deviation.related.hasOwnProperty('more_from_artist') ? Deviation.related.more_from_artist.slice(0, 9) : [],
            da: Deviation.related.hasOwnProperty('more_from_da') ? Deviation.related.more_from_da.slice(0, 9) : [],
            author: Deviation.related.hasOwnProperty('author') ? Deviation.related.author.username : ''
        };
        let userUrl = Deviation.data.hasOwnProperty('author') ? `/profile/${Deviation.data.author.username}` : '';
        return m('.Wrapper.Wrapper__With-Header.Deviation', Deviation.data.hasOwnProperty('deviationid') ? [
            m('.Page__With-Sidebar.Deviation__Content', [
                m('.Deviation__Preview', User.showMatureWarning(Deviation.data.is_mature)
                    ? m('.Mature', [
                        'This deviation contains mature content. Click ',
                        m('a', {onclick: () => {
                            User.mature = true;
                            User.toggleMature();
                        }}, 'here'),
                        ' to show mature content.'
                    ])
                    : (Deviation.data.hasOwnProperty('preview') ? m('img', {
                        src: Deviation.data.preview.src,
                        onclick: () => {
                            Deviation.showFullView = true;
                        }
                    }) : m('.Deviation__Preview--Literature', [
                        m('h1', Deviation.data.title),
                        Deviation.content.hasOwnProperty('html')
                        ? m.trust(Deviation.content.html) : SVG.loading()
                    ]))
                ),
                m('.Title', [
                    m('.Title__Text', [
                        m(m.route.Link,
                            {href: userUrl},
                            m('img.Avatar', {src: Deviation.data.author.usericon})
                        ),
                        m('span', [
                            m('h1', Deviation.data.title),
                            m('span.name', [
                                'by ',
                                m(m.route.Link, {class: `Icon__Button${User.bannedClass(Deviation.data.author.type, false)}`, href: userUrl}, [
                                    Deviation.data.author.username,
                                    User.getRankIcon(Deviation.data.author.type)
                                ])
                            ])
                        ])
                    ]),
                    User.loggedIn() && Deviation.metadata.hasOwnProperty('collections') ? m('.Buttons', [
                        m('button.Icon__Button.fave', {
                            disabled: Deviation.postingFave,
                            onclick: () => {
                                Deviation.postFave(Deviation.metadata.collections.length > 0);
                            }
                        }, Deviation.metadata.collections.length > 0
                            ? [SVG.icon('unfave'), m('span', 'Unfave')]
                            : [SVG.icon('fave'), m('span', 'Fave')]
                        ),
                        m('button.collection.Icon__Button', [
                            m('span.title', {onclick: () => {
                                if (!Deviation.loadingCollections && Collections.data.length === 0) {
                                    Deviation.fetchCollections();
                                }
                                Deviation.showCollection = !Deviation.showCollection;
                            }}, [SVG.icon('folder'), m('span', 'Collection+')]),
                            Deviation.showCollection ? m('.Dropdown', [
                                Collections.data.length > 0
                                ? m('ul.List.List__Sidebar', Collections.data.map((collection, index) => {
                                    return m('li', m('span', [
                                        m('input', {
                                            type: 'checkbox',
                                            checked: Collections.selected[index],
                                            id: collection.folderid,
                                            onclick: (e) => {
                                                Collections.selected[index] = e.currentTarget.checked;
                                            }
                                        }),
                                        m('label', {for: collection.folderid}, collection.name)
                                    ]));
                                })) : m('.Loading', SVG.loading()),
                                m('.Buttons', [
                                    m('button', {
                                        disabled: Deviation.postingFave,
                                        onclick: () => {
                                            Collections.reset();
                                        }
                                    }, 'Reset'),
                                    m('button', {
                                        disabled: Deviation.postingFave,
                                        onclick: () => {
                                            Deviation.postCollections();
                                        }
                                    }, 'Save'),
                                    Collections.hasMore
                                    ? m('button', {disabled: Deviation.loadingCollections}, Locale.loadmore) : ''
                                ])
                            ]) : ''
                        ]),
                        m('button.Icon__Button.watch', {
                            disabled: !Deviation.watchStatus || Deviation.postingWatch,
                            onclick: () => {
                                Deviation.postWatch(Deviation.watching);
                            }
                        }, [
                            Deviation.watching ? SVG.icon('unwatch') : SVG.icon('watch'),
                            m('span', `${Locale.watchbutton}${Deviation.watching ? '-' : '+'}`)
                        ])
                    ]) : (Deviation.metadata.hasOwnProperty('error')
                        ? m('span', Deviation.metadata.error_description) : '')
                ]),
                Deviation.metadata.hasOwnProperty('description') ?
                m('.Description', m.trust(Deviation.metadata.description))
                : (Deviation.metadata.hasOwnProperty('error')
                    ? m('span', Deviation.metadata.error_description) : m('.Loading', SVG.loading())
                ),
                m('.Related', [
                    m('hr'),
                    Deviation.related.hasOwnProperty('author')
                    ? m(Related, {...related, accordion: true}) : m('.Loading', SVG.loading())
                ]),
                m('hr'),
                Deviation.data.allows_comments ? m(Reply, {
                    loading: Deviation.postingComment,
                    postCommentFn: Deviation.postComment,
                    setCommentFn: Deviation.setCommentEdit,
                    commentEdit: Deviation.commentEdit
                }) : m('span.comments-disabled', '(Comments disabled)'),
                Deviation.comments.main.hasOwnProperty('thread')
                ? (Deviation.comments.main.thread.length > 0 ? [m(Comments, {
                    key: `${Deviation.page.index}${Deviation.postedCommentId}`,
                    thread: Deviation.comments.main.thread,
                    page: Deviation.page,
                    changePageFn: Deviation.changePage,
                    replies: Deviation.comments.replies,
                    postCommentFn: Deviation.postComment,
                    loading: Deviation.comments.loading,
                    replyEnabled: Deviation.data.allows_comments
                })] : m('span.nocomments.footer-padding', '(No comments)'))
                : m('.Loading.footer-padding', SVG.loading())
            ]),
            m('.Menu.Menu__Fluid.Deviation__Column', [
                Deviation.data.hasOwnProperty('author') ? m('.Title', [
                    m('span', [
                        m('h1', Deviation.data.title),
                        m('span.name', [
                            'by ',
                            m(m.route.Link, {class: `Icon__Button${User.bannedClass(Deviation.data.author.type, false)}`, href: userUrl}, [
                                Deviation.data.author.username,
                                User.getRankIcon(Deviation.data.author.type)
                            ])
                        ])
                    ]),
                    m('.Menu__Option.info.Icon', {onclick: () => {
                        Deviation.showDetails = !Deviation.showDetails;
                    }}, [SVG.icon('info'), m('div', 'Information')])
                ]) : '',
                m(`.Info${Deviation.showDetails ? '' : '.hidden-sm'}`, [
                    Deviation.metadata.hasOwnProperty('submission') ? [
                        m('h3.Icon__Thumbnail', [SVG.icon('info'), m('span', 'Details')]),
                        m('ul.Details', [
                            m('li.Info__Item', [
                                m('h4', 'Submitted on'),
                                m('span', DateFormatter.formatFromString(Deviation.metadata.submission.creation_time))
                            ]),
                            m('li.Info__Item', [
                                m('h4', 'Category'), m('span', Deviation.metadata.submission.category)
                            ]),
                            Deviation.data.hasOwnProperty('preview') ? [
                                m('li.Info__Item', [
                                    m('h4', 'Image Size'),
                                    m('span', Deviation.metadata.submission.file_size)
                                ]),
                                m('li.Info__Item', [
                                    m('h4', 'Resolution'),
                                    m('span', Deviation.metadata.submission.resolution)
                                ]),
                            ] : '',
                            Deviation.data.is_downloadable ? m('li.Info__Item', m('button.Icon__Thumbnail', {onclick: () => {
                                Deviation.fetchDownload();
                            }}, [SVG.icon('download'), m('span', 'Download')])) : ''
                        ]),
                        m('hr'),
                        m('h4.Icon__Thumbnail', [SVG.icon('link'), m('span', 'Link')]),
                        m(Copy, {
                            text: `http://fav.me/d${Base36.encode(Deviation.extractEmbedID(Deviation.data.url))}`
                        }),
                        m('h4.Icon__Thumbnail', [SVG.icon('image'), m('span', 'Thumb')]),
                        m(Copy, {text: `:thumb${Deviation.extractEmbedID(Deviation.data.url)}:`})
                    ] : (Deviation.metadata.hasOwnProperty('error')
                        ? m('h3', Deviation.metadata.error_description) : m('.Loading', SVG.loading())
                    ),
                    Deviation.data.hasOwnProperty('url') ? [
                        m('h3.Icon__Thumbnail', [SVG.icon('share'), m('span', 'Share')]),
                        m('.share', [
                            m('a.Icon', {
                                href: `https://facebook.com/sharer/sharer.php?u=${encodeURIComponent(Deviation.data.url)}`,
                                target: '_blank'
                            }, SVG.icon('facebook')),
                            m('a.Icon', {
                                href: `https://twitter.com/intent/tweet/?text=${encodeURIComponent(Deviation.shareText)}&amp;url=${encodeURIComponent(Deviation.data.url)}`,
                                target: '_blank'
                            }, SVG.icon('twitter')),
                            m('a.Icon', {
                                href: `https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=${encodeURIComponent(Deviation.data.title)}&amp;caption=${encodeURIComponent(Deviation.shareText)}&amp;content=${encodeURIComponent(Deviation.data.url)}&amp;canonicalUrl=${encodeURIComponent(Deviation.data.url)}&amp;shareSource=tumblr_share_button`,
                                target: '_blank'
                            }, SVG.icon('tumblr')),
                            m('a.Icon', {
                                href: `https://pinterest.com/pin/create/button/?url=${encodeURIComponent(Deviation.data.url)}&amp;media=${encodeURIComponent(Deviation.data.url)}&amp;description=${encodeURIComponent(Deviation.shareText)}`,
                                target: '_blank'
                            }, SVG.icon('pinterest'))
                        ])
                    ] : '',
                    m('hr'),
                    Deviation.metadata.hasOwnProperty('stats') ? [
                        m('h3.Icon__Thumbnail', [SVG.icon('stats'), m('span', 'Stats')]),
                        m('ul.Stats', [
                            m('li.Info__Item', [
                                m('h4', 'Views'),
                                m('span', Deviation.metadata.stats.views),
                                m('span', `(${Deviation.metadata.stats.views_today} today)`)
                            ]),
                            m('li.Info__Item.faves', m(Accordion, {
                                disabled: Deviation.metadata.stats.favourites === 0 || (Deviation.favorites.data.length === 0 && Deviation.favorites.hasMore),
                                header: [
                                    m('h4', 'Favorites'),
                                    m('span', Deviation.metadata.stats.favourites),
                                    Deviation.metadata.stats.favourites > 0 ? m('a', {onclick: () => {
                                        if (!Deviation.favorites.loading && Deviation.favorites.data.length === 0) {
                                            Deviation.fetchFaves();
                                        }
                                    }}, '(who?)') : ''],
                                selector: '.Favorites',
                                content: [
                                    m('ul.List__User', Deviation.favorites.data.length > 0
                                        ? Deviation.favorites.data.map((item) => {
                                            return m('li', m(m.route.Link,
                                                {href: `/profile/${item.user.username}`}, [
                                                    m('img', {src: item.user.usericon}),
                                                    m(`span.Icon__Button${User.bannedClass(item.user.type)}`, [item.user.username, User.getRankIcon(item.user.type)])
                                                ]));
                                        }) : ''),
                                    Deviation.favorites.loading ? m('.Loading', SVG.loading()) : '',
                                    Deviation.favorites.data.length > 0 && Deviation.favorites.hasMore
                                    ? m('button', {
                                        disabled: Deviation.favorites.loading,
                                        onclick: () => {
                                            Deviation.fetchFaves();
                                        }}, Locale.loadmore) : ''
                                ]
                            })),
                            m('li.Info__Item', [
                                m('h4', 'Comments'),
                                m('span', Deviation.metadata.stats.comments)
                            ]),
                            Deviation.data.is_downloadable ? m('li.Info__Item', [
                                m('h4', 'Downloads'),
                                m('span', Deviation.metadata.stats.downloads),
                                m('span', `(${Deviation.metadata.stats.downloads_today} today)`)
                            ]) : '',
                        ])
                    ] : (Deviation.metadata.hasOwnProperty('error')
                        ? m('h3', Deviation.metadata.error_description) : m('.Loading', SVG.loading())
                    ),
                    m('hr')
                ]),
                m('.Related--Column', Deviation.related.hasOwnProperty('author') ? m(Related, {
                    ...related,
                    accordion: false
                }) : m('.Loading', SVG.loading()))
            ]),
            Deviation.data.hasOwnProperty('content') && Deviation.showFullView
            ? m('.Overlay.FullView', [
                m('.close', {onclick: () => {
                    Deviation.showFullView = false;
                }}, SVG.icon('close')),
                m('.Scroll-Container', m('img', {src: Deviation.data.content.src}))
            ]) : ''
        ] : m('.Loading.Loading__Overlay.Loading__Overlay--Left', Deviation.data.hasOwnProperty('error')
            ? Deviation.data.error_description : SVG.loading())
        );
    }
};
