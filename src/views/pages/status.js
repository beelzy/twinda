const m = require('mithril');

const Status = require('../../models/status.js');
const Comments = require('../components/comments.js');
const Reply = require('../components/reply.js');
const Tabs = require('../components/tabs.js');
const SVG = require('../../utils/svg.js');
const DateFormatter = require('../../utils/date.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    oninit: (vnode) => {
        Status.init(vnode.attrs.id);
        Status.fetchComments();
    },
    view: (vnode) => {
        return m('.Wrapper.Wrapper__Tabbed.Wrapper__With-Header.Status', [ 
            m(Tabs),
            m('.Profile__Bar.Profile__Bar--Title', Status.data.hasOwnProperty('ts')
                ? [
                    m('h1.title', `${DateFormatter.formatFromString(Status.data.ts)} Status`),
                    m('.Buttons', m(m.route.Link, {href: `/statuses/${Status.data.author.username}`},
                        m('button', 'More Statuses')
                    ))
                ] : (Status.loading ? m('.Profile__Bar--ScrollWrapper', m('.Loading', SVG.loading()))
                    : m('span', Status.data.error_description)),
            ),
            m('.Content.Content__Tabbed', m('.Page.Page__Tabbed', [
                m('.Entry', m('.Entry__Content', Status.data.hasOwnProperty('body')
                    ? m.trust(Status.data.body) : (Status.loading ? m('.Loading', SVG.loading()) : '')
                )),
                m(Reply, {
                    loading: Status.postingComment,
                    postCommentFn: Status.postComment,
                    setCommentFn: Status.setCommentEdit,
                    commentEdit: Status.commentEdit
                }),
                Status.comments.main.hasOwnProperty('thread')
                ? (Status.comments.main.thread.length > 0 ? [m(Comments, {
                    key: `${Status.page.index}${Status.postedCommentId}`,
                    thread: Status.comments.main.thread,
                    page: Status.page,
                    changePageFn: Status.changePage,
                    replies: Status.comments.replies,
                    postCommentFn: Status.postComment,
                    loading: Status.comments.loading,
                    replyEnabled: true,
                    nofooter: true
                })] : m('span.nocomments', Locale.nocomments))
                : m('.Loading', SVG.loading())
            ])),
        ]);
    }
};
