const m = require('mithril');

const SVG = require('../../utils/svg.js');
const User = require('../../models/user.js');

module.exports = {
    view: (vnode) => {
        let bgclass = vnode.attrs.size !== 'Large' ? '.Icon__Thumbnail--Background' : '';
        let stats = vnode.attrs.deviation.hasOwnProperty('stats') ? [
            m(`span.Icon__Thumbnail${bgclass}.faves`, [SVG.icon('fave'), m('span', vnode.attrs.deviation.stats.favourites)]),
            m(`span.Icon__Thumbnail${bgclass}.comments`, [SVG.icon('comment'), m('span', vnode.attrs.deviation.stats.comments)])
        ] : '';
        let literature = m('.literature', m('.content', m.trust(vnode.attrs.deviation.excerpt)));
        return [
            m(`div.Thumbnail${vnode.attrs.size ? `.Thumbnail__${vnode.attrs.size}` : ''}${vnode.attrs.selector ? vnode.attrs.selector : ''}`,
                m(m.route.Link, {
                    href: vnode.attrs.link ? vnode.attrs.link
                    : `/deviation/${vnode.attrs.deviation.deviationid}`
                }, [
                    vnode.attrs.notitle ? ''
                    : m(`${vnode.attrs.size === 'Large' ? 'h4' : ''}.title`, vnode.attrs.deviation.title),
                    m('.Thumbnail__Content', [
                        vnode.attrs.deviation.hasOwnProperty('thumbs')
                        || vnode.attrs.deviation.hasOwnProperty('excerpt')
                        ? (vnode.attrs.deviation.thumbs.length > 0 ? [
                            m('img', {src: vnode.attrs.deviation.thumbs[1].src}),
                            vnode.attrs.deviation.hasOwnProperty('excerpt') ? literature : ''
                        ] : literature) : m('h1', 'No Preview'),
                        vnode.attrs.size === 'Large' || vnode.attrs.nostats ? ''
                        : m('.stats', stats)
                    ]),
                    vnode.attrs.nouser ? '' : m('.user', [
                        'by ',
                        m(m.route.Link, {class: `Icon__Button${User.bannedClass(vnode.attrs.deviation.author.type, false)}`, href: `/profile/${vnode.attrs.deviation.author.username}`}, [
                            vnode.attrs.deviation.author.username,
                            User.getRankIcon(vnode.attrs.deviation.author.type)
                        ])
                    ])
                ])
            ),
            vnode.attrs.size === 'Large' && !vnode.attrs.nostats ? m('.Thumbnail__Stats', stats) : ''
        ];
    }
};
