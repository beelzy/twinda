const m = require('mithril');

const SVG = require('../../utils/svg.js');

module.exports = class Accordion {
    constructor(vnode) {
        this.accordionHeight = 0;
        this.transitioning = false;
        this.isOpen = vnode.attrs.open === true;
        this.setHeight = false;
    }

    onupdate(vnode) {
        if (this.setHeight) {
            this.setHeight = false;
            this.accordionHeight = 0;
            m.redraw();
        }
    }

    view(vnode) {
        this.isOpen = vnode.attrs.disabled ? true : this.isOpen;
        let accordionStyle = this.transitioning || !this.isOpen ? {style: `height: ${this.accordionHeight}px;`} : {};
        return [
            m(`.Accordion__Header${vnode.attrs.hasOwnProperty('headselector') ? vnode.attrs.headselector : ''}${this.isOpen ? '.opened' : ''}${vnode.attrs.disabled ? '.disabled' : ''}`,
                {onclick: (e) => {
                    if (!vnode.attrs.disabled) {
                        this.transitioning = true;
                        this.accordionHeight = e.currentTarget.nextSibling.scrollHeight;
                        if (this.isOpen) {
                            this.setHeight = true;
                        }
                        this.isOpen = !this.isOpen;
                    }
                }}, [vnode.attrs.header, m('.Icon.Accordion__Header--Icon', SVG.icon('arrow'))]),
            m(`.Accordion${vnode.attrs.hasOwnProperty('selector') ? vnode.attrs.selector : ''}`,
                {...accordionStyle, ontransitionend: () => {
                    this.transitioning = false;
                }}, vnode.attrs.content)
        ];
    }
};
