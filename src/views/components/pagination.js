const m = require('mithril');

const SVG = require('../../utils/svg.js');

let skipIndex = 0;
let skipFocused = false;

let changePage = (page, fn) => {
    skipIndex = page + 1;
    fn(page);
};

module.exports = {
    oninit: (vnode) => {
        skipIndex = vnode.attrs.index + 1;
    },
    view: (vnode) => {
        let pages = Math.min(vnode.attrs.pages, vnode.attrs.total);
        let mobilePages = Math.min(vnode.attrs.mobilePages, vnode.attrs.total);
        let increment = Math.max(1, Math.floor(pages / 2));
        let mobileIncrement = Math.max(1, Math.floor(mobilePages / 2));
        let pageOffset = Math.max(0, vnode.attrs.index - increment);
        let limit = Math.min(vnode.attrs.index + increment, vnode.attrs.total - 1);
        pageOffset = Math.max(0, pageOffset - (vnode.attrs.index + increment - limit));
        let mobileOffset = Math.max(0, -1 * (vnode.attrs.index - mobileIncrement));
        mobileOffset += Math.min(0, -1 * (vnode.attrs.index + mobileIncrement - limit));
        return m(`.Pagination${vnode.attrs.nofooter ? '' : '.footer-padding'}${vnode.attrs.selector ? vnode.attrs.selector : ''}`,
            m('.Pagination__Wrapper', [
                m('button.rewind.Icon__Flip.Icon__Double', {
                    disabled: vnode.attrs.index === 0,
                    onclick: () => {
                        changePage(0, vnode.attrs.changePageFn);
                    }
                }, [SVG.icon('angle'), SVG.icon('angle')]),
                m('button.previous.Icon__Flip', {
                    disabled: vnode.attrs.index === 0,
                    onclick: () => {
                        changePage(Math.max(0, vnode.attrs.index - 1), vnode.attrs.changePageFn);
                    }
                }, SVG.icon('angle')),
                vnode.attrs.pages ? Array(pages).fill().map((_, i) => {
                    let index = i + pageOffset;
                    return m(`button.page${Math.abs(index - vnode.attrs.index - mobileOffset) > mobileIncrement ? '.hidden-sm' : ''}${index === vnode.attrs.index ? '.selected' : ''}`, {
                        onclick: () => {
                            if (index !== vnode.attrs.index) {
                                changePage(index, vnode.attrs.changePageFn);
                            }
                        }
                    }, index + 1);
                }) : '',
                m('.skip-container', [
                    skipFocused ? m('input', {
                        type: 'number',
                        min: 1, max: vnode.attrs.total,
                        value: skipIndex,
                        oninput: (e) => {
                            skipIndex = Math.max(Math.min(parseInt(e.currentTarget.value), vnode.attrs.total), 0);
                        }
                    }) : '',
                    m(`button.skip${skipFocused ? '.active' : ''}`, {
                        onclick: () => {
                            if (skipFocused && skipIndex -1 !== vnode.attrs.index) {
                                changePage(skipIndex - 1, vnode.attrs.changePageFn);
                                skipFocused = false;
                            } else {
                                skipFocused = !skipFocused;
                            }
                        }
                    }, skipFocused ? 'Go' : '...')
                ]),
                m('button.next', {
                    disabled: vnode.attrs.index >= vnode.attrs.total - 1,
                    onclick: () => {
                        changePage(Math.min(vnode.attrs.total - 1, vnode.attrs.index + 1),
                            vnode.attrs.changePageFn);
                    }
                }, SVG.icon('angle')),
                m('button.fastforward.Icon__Double', {
                    disabled: vnode.attrs.index >= vnode.attrs.total - 1,
                    onclick: () => {
                        changePage(vnode.attrs.total - 1, vnode.attrs.changePageFn);
                    }
                }, [SVG.icon('angle'), SVG.icon('angle')])
            ])
        );
    }
};
