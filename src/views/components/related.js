const m = require('mithril');

const Accordion = require('./accordion.js');
const Thumbnail = require('./thumbnail-small.js');

module.exports = {
    view: (vnode) => {
        return [
            vnode.attrs.artist.length > 0 ? m(Accordion, {
                disabled: !vnode.attrs.accordion,
                header: m('h3', `More from ${vnode.attrs.author}`),
                content: [
                    m(`ul.Thumbnails${vnode.attrs.accordion ? '' : '.Thumbnails__Menu'}`,
                        vnode.attrs.artist.map((deviation) => {
                            return m(Thumbnail, {deviation: deviation});
                        })
                    ),
                    m(m.route.Link, {
                        class: 'show-more',
                        href: `/gallery/${vnode.attrs.author}`
                    }, 'View Gallery')
                ]
            }) : '',
            m('hr'),
            vnode.attrs.da.length > 0 ? m(Accordion, {
                disabled: !vnode.attrs.accordion,
                header: m('h3', 'More from DeviantArt'),
                content: m(`ul.Thumbnails${vnode.attrs.accordion ? '' : '.Thumbnails__Menu'}`,
                    vnode.attrs.da.map((deviation) => {
                        return m(Thumbnail, {deviation: deviation});
                    })
                )
            }) : ''
        ];
    }
};
