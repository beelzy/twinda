const m = require('mithril');

const DateFormatter = require('../../utils/date.js');
const SVG = require('../../utils/svg.js');
const User = require('../../models/user.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    view: (vnode) => {
        let isJournal = vnode.attrs.model === 'journal';
        let link = isJournal ? `/deviation/${vnode.attrs.entry.deviationid}` : `/status/${vnode.attrs.entry.statusid}`;
        return m('.Entry', [
            m('.Entry__Title', [
                m('.title', [
                    m(m.route.Link, {href: link},
                        m('h1', isJournal ? vnode.attrs.entry.title : DateFormatter.formatFromString(vnode.attrs.entry.ts)
                        )
                    ),
                    isJournal ? m('.date', DateFormatter.formatFromTimestamp(vnode.attrs.entry.published_time)) : ''
                ]),
                m('.info', [
                    m('.user', [
                        'by ',
                        m(m.route.Link, {class: `Icon__Button${User.bannedClass(vnode.attrs.entry.author.type, false)}`, href: `/profile/${vnode.attrs.entry.author.username}`}, [
                            vnode.attrs.entry.author.username,
                            User.getRankIcon(vnode.attrs.entry.author.type)
                        ])
                    ]),
                    isJournal ? m('.category', vnode.attrs.entry.category_path) : ''
                ])
            ]),
            m('hr'),
            m('.Entry__Content', [
                m.trust(isJournal ? vnode.attrs.entry.excerpt : vnode.attrs.entry.body),
                '...'
            ]),
            m('hr'),
            m('.Entry__Footer', [
                m('.Entry__Footer--Stats', [
                    isJournal ? m('span.Icon__Thumbnail', [SVG.icon('fave'), m('span', vnode.attrs.entry.stats.favourites)]) : '',
                    m('span.Icon__Thumbnail', [SVG.icon('comment'), m('span', isJournal
                        ? vnode.attrs.entry.stats.comments : vnode.attrs.entry.comments_count)
                    ])
                ]),
                m(m.route.Link, {
                    class: 'Entry__Footer--Button',
                    href: link
                }, m('button', isJournal ? Locale.journalExpand : Locale.statusExpand))
            ])
        ]);
    }
};
