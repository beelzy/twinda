const m = require('mithril');

const DateFormatter = require('../../utils/date.js');
const User = require('../../models/user.js');
const Locale = require('../../utils/locale.js');

module.exports = class Comment {
    constructor(vnode) {
        this.showReply = false;
        this.commentEdit = '';
        this.posting = false;
    }

    oninit(vnode) {
        this.showReply = false;
        this.commentEdit = '';
        this.posting = false;
    }

    view(vnode) {
        let url = `/profile/${vnode.attrs.comment.user.username}`;
        return m('.Comment', [
            m(m.route.Link, {class: 'Avatar hidden-sm', href: url},
                m('img', {src: vnode.attrs.comment.user.usericon})),
            m('.Comment__Body', [
                m('.Comment__Title', [
                    m(`span.user.Icon__Button${User.bannedClass(vnode.attrs.comment.user.type)}`, m(m.route.Link, {href: url}, [
                        vnode.attrs.comment.user.username,
                        User.getRankIcon(vnode.attrs.comment.user.type)
                    ])),
                    m('span.date', DateFormatter.formatFromString(vnode.attrs.comment.posted))
                ]),
                m('hr'),
                m('span.Comment__Content', m.trust(vnode.attrs.comment.body)),
                vnode.attrs.replyEnabled ? (User.loggedIn() ? m('.Reply', [
                    this.showReply ? m('textarea', {
                        oninput: (e) => {
                            this.commentEdit = e.currentTarget.value;
                        },
                        placeholder: Locale.replyplaceholder,
                        value: this.commentEdit
                    }) : '',
                    m('.Buttons', [
                        this.showReply ? m('button', {onclick: () => {
                            this.showReply = false;
                        }}, 'Close') : '',
                        m('button', {
                            disabled: this.posting,
                            onclick: () => {
                                if (this.showReply) {
                                    this.posting = true;
                                    vnode.attrs.postCommentFn({
                                        body: this.commentEdit,
                                        commentid: vnode.attrs.comment.commentid
                                    }).then(() => {
                                        this.posting = false;
                                        this.commentEdit = '';
                                        this.showReply = false;
                                        m.redraw();
                                    });
                                } else {
                                    this.showReply = true;
                                }
                            }
                        }, this.showReply ? 'Submit' : Locale.replybutton)
                    ])
                ]) : '') : m('span.comments-disabled', Locale.commentsDisabled)
            ])
        ]);
    }
};
