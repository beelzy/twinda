const m = require('mithril');

module.exports = {
    view: (vnode) => {
        return m(`li${vnode.attrs.deviation.hasOwnProperty('excerpt') > 0 ? '.literature' : ''}`,
            m(m.route.Link, {href: `/deviation/${vnode.attrs.deviation.deviationid}`},
                vnode.attrs.deviation.thumbs.length > 0 ?
                m('.Thumbnails__Image',
                    {style: `background-image: url("${vnode.attrs.deviation.thumbs[0].src}");`},
                    vnode.attrs.deviation.hasOwnProperty('excerpt') ? m('.bg', m('.excerpt', [
                        m('h1', vnode.attrs.deviation.title),
                        m('.content', m.trust(vnode.attrs.deviation.excerpt))
                    ])) : '') : m('.excerpt', [
                        m('h1', vnode.attrs.deviation.title),
                        m('.content', m.trust(vnode.attrs.deviation.excerpt))
                    ])
            )
        );
    }
};
