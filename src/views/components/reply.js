const m = require('mithril');

const User = require('../../models/user.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    view: (vnode) => {
        return User.loggedIn() ? m('.Reply', [
            m('textarea', {oninput: (e) => {
                vnode.attrs.setCommentFn(e.currentTarget.value);
            }, placeholder: Locale.replyplaceholder, value: vnode.attrs.commentEdit}),
            m('button', {disabled: vnode.attrs.loading, onclick: vnode.attrs.postCommentFn}, Locale.replybutton)
        ]) : m('span.login', [
            'Please ', m('a', {href: `/authorize?route=${m.route.get()}`}, 'log in'), ' to reply'
        ]);
    }
};
