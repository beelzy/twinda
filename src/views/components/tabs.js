const m = require('mithril');

const Tabs = require('../../models/tabs.js');
const User = require('../../models/user.js');
const SVG = require('../../utils/svg.js');
const Locale = require('../../utils/locale.js');

module.exports = {
    view: (vnode) => {
        let profileActive = m.route.get() ? m.route.get().startsWith('/profile') : false;
        let profile = [SVG.icon('user'), m('span.hidden-sm', 'Profile')];
        let galleryActive = m.route.get() ? m.route.get().startsWith('/gallery') : false;
        let gallery = [SVG.icon('images'), m('span.hidden-sm', 'Gallery')];
        let collectionsActive = m.route.get() ? m.route.get().startsWith('/collections') : false;
        let collections = [SVG.icon('fave'), m('span.hidden-sm', 'Collections')];
        let journalActive = m.route.get() ? m.route.get().startsWith('/journal') : false;
        let journal = [SVG.icon('book'), m('span.hidden-sm', 'Journal')];
        return m('.Profile__Tab', [
            Tabs.data.hasOwnProperty('user') ? [
                m('.Profile__Username', [
                    m('img.Avatar', {src: Tabs.data.user.usericon}),
                    m(`h1${User.bannedClass(Tabs.data.user.type)}`, [Tabs.data.user.username, User.getRankIcon(Tabs.data.user.type)])
                ]),
                m('.Profile__Title', [
                    Tabs.data.tagline || Tabs.data.real_name ? m('tagline', Tabs.data.tagline === '' ? Tabs.data.real_name : Tabs.data.tagline) : '',
                    Tabs.data.hasOwnProperty('user_is_artist') && Tabs.data.user_is_artist ? m('.role', `Artist${Tabs.data.artist_level === 'None' ? '' : ` / ${Tabs.data.artist_level}`}${Tabs.data.artist_specialty === 'None' ? '' : ` / ${Tabs.data.artist_specialty}`}`) : '',
                ])
            ] : [m('.Profile__Username', Tabs.data.hasOwnProperty('error') ? m('h1', Tabs.data.error_description) : m('h1.Loading', SVG.loading())), m('.Profile__Title', '')],
            m('.Tabs', [
                m(`.Tab.Icon.Icon__Tab${profileActive ? '.active' : ''}`,
                    profileActive ? profile : m(m.route.Link,
                        {href: `/profile/${Tabs.username}`}, profile)),
                m(`.Tab.Icon.Icon__Tab${galleryActive ? '.active' : ''}`,
                    galleryActive ? gallery : m(m.route.Link,
                        {href: `/gallery/${Tabs.username}`}, gallery)),
                m(`.Tab.Icon.Icon__Tab${collectionsActive ? '.active' : ''}`,
                    collectionsActive ? collections : m(m.route.Link,
                        {href: `/collections/${Tabs.username}`}, collections)),
                m(`.Tab.Icon.Icon__Tab${journalActive ? '.active' : ''}`,
                    journalActive ? journal : m(m.route.Link,
                        {href: `/journal/${Tabs.username}`}, journal))
            ]),
            User.loggedIn() && !Tabs.isSameUser() && !Tabs.data.hasOwnProperty('error')
            ? m('.Buttons', [
                m('button.Icon__Button', {
                    onclick: () => {
                        Tabs.postWatch(Tabs.isWatching);
                    },
                    disabled: !Tabs.watchStatus || Tabs.postingWatch
                }, [
                    Tabs.isWatching ? SVG.icon('unwatch') : SVG.icon('watch'),
                    `${Locale.watchbutton}${Tabs.isWatching? '-' : '+'}`
                ]),
                m('button.Icon__Button', [SVG.icon('mail'), 'Note+'])
            ]) : ''
        ]);
    }
};
