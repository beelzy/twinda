const m = require('mithril');

module.exports = {
    view: (vnode) => {
        return m('.Copy', [
            m('input', {type: 'text', value: vnode.attrs.text, readonly: true}),
            m('button', {onclick: (e) => {
                e.currentTarget.previousElementSibling.select();
                document.execCommand('copy');
            }}, 'copy')
        ]);
    }
};
