const m = require('mithril');

const Comment = require('./comment.js');

const CommentTree = {
    view: (vnode) => {
        return m('ul.Comments', vnode.attrs.thread.map((comment) => {
            return m('li', [
                [m(Comment, {
                    replyEnabled: vnode.attrs.replyEnabled,
                    comment: comment,
                    postCommentFn: vnode.attrs.postCommentFn
                })],
                vnode.attrs.replies.hasOwnProperty(comment.commentid) ? [m(CommentTree, {
                    replyEnabled: vnode.attrs.replyEnabled,
                    thread: vnode.attrs.replies[comment.commentid],
                    replies: vnode.attrs.replies,
                    postCommentFn: vnode.attrs.postCommentFn
                })] : ''
            ]);
        }));
    }
};

module.exports = CommentTree;
