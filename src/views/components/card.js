const m = require('mithril');

const Accordion = require('./accordion.js');
const SVG = require('../../utils/svg.js');

module.exports = {
    view: (vnode) => {
        return m('.Card', m(Accordion, {
            selector: '.Card__Content',
            headselector: '.Card__Title.Icon',
            header: [vnode.attrs.icon ? SVG.icon(vnode.attrs.icon) : '', vnode.attrs.header],
            open: vnode.attrs.open,
            content: m('.Card__Content--Wrapper', vnode.attrs.content)
        }));
    }
};
