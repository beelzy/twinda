const m = require('mithril');

const Modal = require('../../models/modal.js');

module.exports = {
    view: (vnode) => {
        return Modal.messages.length > 0 ? m(`.Notifications`, Modal.messages.map((message) => {
            return m('.Notifications__Message', message);
        })) : '';
    }
};
