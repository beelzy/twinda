const m = require('mithril');

const Pagination = require('./pagination.js');
const CommentTree = require('./commenttree.js');
const SVG = require('../../utils/svg.js');

module.exports = {
    view: (vnode) => {
        let commenttree = [
            vnode.attrs.loading ? m('.Loading__Sticky', SVG.loading()) : '',
            [m(CommentTree, {
                replyEnabled: vnode.attrs.replyEnabled,
                postCommentFn: vnode.attrs.postCommentFn,
                thread: vnode.attrs.thread,
                replies: vnode.attrs.replies
            })]
        ];
        return [
            vnode.attrs.fixedPagination ? m('.Comments__ScrollWrapper', commenttree) : commenttree,
            m(Pagination, {
                index: vnode.attrs.page.index,
                total: vnode.attrs.page.total,
                mobilePages: vnode.attrs.page.cacheMobile,
                pages: vnode.attrs.page.cache,
                changePageFn: vnode.attrs.changePageFn,
                nofooter: vnode.attrs.nofooter
            })
        ];
    }
};
