const m = require('mithril');

const Request = require('../../utils/request.js');
const User = require('../../models/user.js');
const SVG = require('../../utils/svg.js');
const Collections = require('../../models/user-collections.js');

let notificationsEnabled = false;

module.exports = {
    oninit: (vnode) => {
        User.init();
        User.fetchUser();
    },
    view: (vnode) => {
        let nsfwButton = m('.nsfw', {onclick: () => {
            User.mature = !User.mature;
            User.toggleMature();
        }}, User.mature ? 'On' : 'Off');
        return m('header', [
            m('span.Buttons.Header__Buttons', [
                m(m.route.Link, {href: '/', class: 'Icon__Logo'}, [
                    SVG.icon('logo'),
                    m('span.title.hidden-sm', m('span', 'This Website Is Not'), m('span.deviantart', 'DeviantArt'))
                ]),
                m(m.route.Link, {href: '/about', class: 'Header__Logo'}, [SVG.icon('info'), m('.Header__Logo--Title', 'About')]),
                m('a', {href: 'https://deviantart.com', class: 'Header__Logo'}, [SVG.icon('deviantart'), m('.Header__Logo--Title', 'DeviantArt')]),
                m('a', {href: 'https://gitlab.com/beelzy/twinda', class: 'Header__Logo', target: '_blank'}, [SVG.icon('gitlab'), m('.Header__Logo--Title', 'Source Code')])
            ]),
            User.userLoading ? m('.User__Buttons.Loading', SVG.loading()) : m('span.Buttons.User__Buttons', User.loggedIn() ? [
                m(m.route.Link, {href: `/profile/${User.data.username}`}, [
                    m('img.Avatar', {src: User.data.usericon}),
                    m('.title.Icon__Button', [User.data.username, User.getRankIcon(User.data.type)])
                ]),
                m(`.notifications${notificationsEnabled ? '.hover' : ''}`, {
                    onclick: () => {
                        notificationsEnabled = !notificationsEnabled;
                    },
                    onmouseleave: () => {
                        notificationsEnabled = false;
                    }
                }, [
                    User.notifications.hasOwnProperty('items') || User.notifications.hasOwnProperty('error') ? [
                        m('.Header__Logo', [SVG.icon('inbox'), m('.Header__Logo--Title', 'Notifications')]),
                        m('ul.feed', User.notifications.hasOwnProperty('error')
                            ? m('li', User.notifications.error_description)
                            : User.notifications.items.map((item) => {
                                return m('li', [
                                    m('span.type', item.type),
                                    m('span.user', `by ${item.by_user.username}`)
                                ]);
                            })
                        )
                    ] : m('.Loading', SVG.loading())
                ]),
                nsfwButton,
                m('a.Header__Logo', {onclick: () => {
                    Request.post('/revoke').then((response) => {
                        User.setLoggedIn(false);
                        Collections.clear();
                        m.redraw();
                    });
                }}, [SVG.icon('logout'), m('.Header__Logo--Title', 'Logout')])
            ] : [
                nsfwButton,
                m('a.Header__Logo', {
                    href: `/authorize?route=${m.route.get()}`
                }, [SVG.icon('login'), m('.Header__Logo--Title', 'Login')])
            ])
        ]);
    }
};
