const m = require('mithril');

require('./scss/styles.scss');

const Locale = require('./utils/locale.js');
const Header = require('./views/components/header.js');
const Modal = require('./views/components/modal.js');
const Browse = require('./views/pages/browse.js');
const Deviation = require('./views/pages/deviation.js');
const Profile = require('./views/pages/profile.js');
const Gallery = require('./views/pages/gallery.js');
const Entries = require('./views/pages/entries.js');
const User = require('./models/user.js');
const About = require('./views/pages/about.js');
const Status = require('./views/pages/status.js');
const LocalStorage = require('./utils/localstorage.js');

LocalStorage.init();

const matchRoute = (title) => {
    document.title = title;
    User.queue = [];
};

const mature = () => {
    return User.mature ? '?mature' : '';
};

m.route(document.body, '/', {
    '/': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - Browse Newest`);
        },
        render: () => {
            return [m(Header, {key: 'header'}), m(Browse, {key: `browse/newest${mature()}`}), m(Modal, {key: 'modal'})];
        }
    },
    '/browse/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - Browse ${vnode.key}`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Browse, {key: `browse/${vnode.attrs.key}${mature()}`, id: vnode.attrs.key}), m(Modal, {key: 'modal'})];
        }
    },
    '/browse/topic/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - Browse Topic: ${vnode.key}`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Browse, {key: `browse/topic/${vnode.attrs.key}${mature()}`, id: `topic?topic=${vnode.attrs.key}`}), m(Modal, {key: 'modal'})];
        }
    },
    '/deviation/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - Loading Deviation...`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Deviation, {key: `deviation/${vnode.attrs.key}${mature()}`, id: vnode.attrs.key}), m(Modal, {key: 'modal'})];
        }
    },
    '/profile/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.key}`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Profile, {key: `profile/${vnode.attrs.key}${mature()}`, id: vnode.attrs.key}), m(Modal, {key: 'modal'})];
        }
    },
    '/gallery/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.key} Gallery`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Gallery, {key: `gallery/${vnode.attrs.key}${mature()}`, user: vnode.attrs.key, model: 'gallery'}), m(Modal, {key: 'modal'})];
        }
    },
    '/gallery/:profile/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.profile} ${vnode.key} Gallery`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Gallery, {key: `gallery/${vnode.attrs.profile}/${vnode.attrs.key}${mature()}`, user: vnode.attrs.profile, folderid: vnode.attrs.key, model: 'gallery'}), m(Modal, {key: 'modal'})];
        }
    },
    '/collections/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.key} Collections`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Gallery, {key: `collections/${vnode.attrs.key}${mature()}`, user: vnode.attrs.key, model: 'collections'}), m(Modal, {key: 'modal'})];
        }
    },
    '/collections/:profile/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.profile} ${vnode.key} Collections`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Gallery, {key: `collections/${vnode.attrs.profile}/${vnode.attrs.key}${mature()}`, user: vnode.attrs.profile, folderid: vnode.attrs.key, model: 'collections'}), m(Modal, {key: 'modal'})];
        }
    },
    '/journal/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.key} Journals`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Entries, {key: `journal/${vnode.attrs.key}${mature()}`, user: vnode.attrs.key, model: 'journal'}), m(Modal, {key: 'modal'})];
        }
    },
    '/statuses/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - ${vnode.key} Statuses`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Entries, {key: `statuses/${vnode.attrs.key}${mature()}`, user: vnode.attrs.key, model: 'status'}), m(Modal, {key: 'modal'})];
        }
    },
    '/status/:key': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - Loading Status...`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(Status, {key: `status/${vnode.attrs.key}${mature()}`, id: vnode.attrs.key}), m(Modal, {key: 'modal'})];
        }
    },
    '/about': {
        onmatch: (vnode) => {
            matchRoute(`${Locale.title} - About`);
        },
        render: (vnode) => {
            return [m(Header, {key: 'header'}), m(About, {key: 'about'}), m(Modal, {key: 'modal'})];
        }
    }
});
