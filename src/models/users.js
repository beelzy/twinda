const m = require('mithril');

const Request = require('../utils/request.js');

const Users = {
    Instance: function(options = {}) {
        this.size = options.size ? options.size : 10;
        this.data = [];
        this.index = 0;
        this.loading = false;
        this.hasMore = true;
        this.error = false;
        this.error_description = '';
        this.reset = () => {
            this.index = 0;
            this.data = [];
            this.hasMore = true;
            this.error = false;
            this.error_description = '';
        };
        this.fetch = (options = {}) => {
            this.loading = true;
            let offset = options.offset ? options.offset : this.index;
            let endpoint = options.endpoint ? options.endpoint : Request.user;
            let queryParams = options.query ? options.query : '';
            let queryDelimiter = options.query ? '&' : '?';
            return endpoint(`${options.slug}${queryParams}${queryDelimiter}limit=${this.size}&offset=${this.index}`).then((response) => {
                if (response.data.hasOwnProperty('error')) {
                    this.error = true;
                    this.error_description = response.data.error_description;
                } else {
                    this.data = [...this.data, ...response.data.results];
                    this.hasMore = response.data.has_more;
                    this.index = response.data.next_offset;
                }
                m.redraw();
            }).finally(() => {
                this.loading = false;
            });
        };
    }
};

module.exports = Users;
