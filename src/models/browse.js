const m = require('mithril');
const Request = require('../utils/request.js');
const Pagination = require('./pagination.js');
const User = require('../models/user.js');

const Browse = {
    init: (key='newest') => {
        if (Browse.key !== key || Browse.mature !== User.mature) {
            Browse.selectedMenuItem = '';
            Browse.page.reset();
            Browse.key = key;
            Browse.index = 0;
            Browse.fetchPage();
            Browse.mature = User.mature;
        }
    },
    fetchPage: (options={}) => {
        Browse.loading = true;
        Request.abortRequests('browse');
        let query = options.query ? options.query : Browse.search;
        let offset = options.offset ? options.offset : Browse.page.index * Browse.page.size;
        let queryDelimiter = Browse.key.includes('topic') ? '&' : '?';
        return Request.browse(`${Browse.key}${queryDelimiter}limit=${Browse.page.size}&offset=${offset}${Browse.key === 'popular' || Browse.key === 'newest' ? `&q=${query}` : ''}${User.matureFilter()}`, 'browse').then((response) => {
            Browse.data = response.data;
            if (!Browse.data.hasOwnProperty('results')) {
                Browse.data.results = [];
            }
            if (Browse.data.hasOwnProperty('estimated_total')) {
                Browse.page.total = Math.min(Browse.page.total, Math.ceil(Browse.data.estimated_total / Browse.page.size));
            }
            Browse.page.index = offset / Browse.page.size;

            m.redraw();
        }).finally(() => {
            Browse.loading = false;
        });
    },
    fetchTopics: () => {
        Browse.loadingTopics = true;
        return Request.browse(`topics?offset=${Browse.topicIndex}${User.matureFilter()}`).then((response) => {
            Browse.topics = [...Browse.topics, ...response.data.results];
            Browse.topicIndex += 10;
            Browse.moreTopics = response.data.has_more;
            m.redraw();
        }).finally(() => {
            Browse.loadingTopics = false;
        });
    },
    selectMenuItem: (item) => {
        Browse.selectedMenuItem = Browse.selectedMenuItem === item ? '' : item;
    },
    loadingTopics: false,
    loading: false,
    selectedMenuItem: '',
    moreTopics: true,
    topicIndex: 0,
    topics: [],
    key: '',
    search: '',
    data: {},
    page: new Pagination.Instance(),
    changePage: (page) => {
        Browse.page.index = page;
        Browse.fetchPage();
    },
    mature: false
};

module.exports = Browse;
