const Gallery = require('./gallery-base.js');
const Request = require('../utils/request.js');

module.exports = new Gallery.Instance({endpoint: Request.collections, name: 'collections', flat: true});
