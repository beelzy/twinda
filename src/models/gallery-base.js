const m = require('mithril');

const Request = require('../utils/request.js');
const User = require('./user.js');
const Folders = require('./folders.js');
const Pagination = require('./pagination.js');
const Tabs = require('./tabs.js');

const Gallery = {
    Instance: function(options={}) {
        this.endpoint = options.endpoint ? options.endpoint : Request.gallery;
        this.name = options.name ? options.name : 'gallery';
        this.id = '';
        this.username = '';
        this.data = {};
        this.folders = new Folders.Instance({flat: options.flat});
        this.featuredid = '';
        this.loading = false;
        this.page = new Pagination.Instance();
        this.isPopular = false;
        this.selectedMenuItem = '';
        this.mature = false;
        this.init = (id, user, isModelSame) => {
            this.id = id ? id : 'all';
            Tabs.data = {};
            if (!isModelSame || user !== this.username || this.mature !== User.mature) {
                this.folders.reset();
            }
            if (user !== this.username) {
                Tabs.reset();
            } else {
                this.folders.folderid = this.id;
                this.folders.findParent();
                this.folders.filterDisplayFolders({reset: true});
            }
            this.page.reset();
            this.folders.folderid = this.id;
            this.username = user;
            this.selectedMenuItem = '';
            Tabs.username = user;
            return Tabs.fetchProfile();
        };
        this.fetchFolders = () => {
            return this.folders.fetch({endpoint: this.endpoint, slug: 'folders', query: `?username=${this.username}${User.matureFilter()}`});
        };
        this.fetchGallery = () => {
            this.data = {};
            this.loading = true;
            Request.abortRequests(this.name);
            return this.endpoint(`${this.id === 'all' ? '/' : this.id}?username=${this.username}&limit=${this.page.size}&offset=${this.page.index * this.page.size}&mode=${this.isPopular ? 'popular' : 'newest'}${User.matureFilter()}`, this.name).then((response) => {
                this.data = response.data;
                if (!response.data.hasOwnProperty('error')) {
                    if (!response.data.has_more) {
                        this.page.total = Math.min(this.page.total, this.page.index + 1);
                    }
                }
                m.redraw();
            }).finally(() => {
                this.loading = false;
            });
        };
        this.changePage = (page) => {
            this.page.index = page;
            this.fetchGallery();
        };
        this.togglePopular = (popular) => {
            this.isPopular = popular;
            this.page.reset();
            return this.fetchGallery();
        };
        this.selectMenuItem = (item) => {
            this.selectedMenuItem = this.selectedMenuItem === item ? '' : item;
        };
    }
};

module.exports = Gallery;
