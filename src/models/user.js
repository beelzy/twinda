const m = require('mithril');

const Request = require('../utils/request.js');
const Modal = require('./modal.js');
const SVG = require('../utils/svg.js');
const LocalStorage = require('../utils/localstorage.js');

const User = {
    init: () => {
        if (LocalStorage.available) {
            User.mature = LocalStorage.data.hasOwnProperty('mature') ? LocalStorage.data.mature : User.mature;
        }
        User.notificationsLoading = true;
        return Request.feed('notifications').then((response) => {
            User.notifications = response.data;
            m.redraw();
            User.notificationsLoading = false;
        });
    },
    fetchUser: () => {
        User.userLoading = true;
        return Request.user('whoami').then((response) => {
            User.data = response.data;
            for(let i = 0, size = User.queue.length; i < size; i++) {
                User.queue[i]();
            }
            User.queue = [];
            User.userLoading = false;
            m.redraw();
        });
    },
    isWatching: (user, id) => {
        if (User.loggedIn()) {
            return Request.user(`friends/search?username=${User.data.username}&query=${user}`).then((response) => {
                let isWatching = false;
                if (response.data.hasOwnProperty('results')) {
                    for (let i = 0, size = response.data.results.length; i < size; i++) {
                        if (response.data.results[i].userid === id) {
                            isWatching = true;
                            break;
                        }
                    }
                }
                return Promise.resolve(isWatching);
            });
        }
        return false;
    },
    postWatch: (options={}) => {
        let unwatch = options.unwatch ? options.unwatch : false;
        let data = unwatch ? {} : {
            'watch[friend]': 1,
            'watch[deviations]': 1,
            'watch[journals]': 1,
            'watch[forum_threads]': 1,
            'watch[critiques]': 1,
            'watch[scraps]': 1,
            'watch[activity]': 1,
            'watch[collections]': 1
        };
        return Request.userPost(`friends/${unwatch ? 'unwatch' : 'watch'}/${options.user}`, data).then((response) => {
            if (response.data.success) {
                return Promise.resolve(true);
            }
            Modal.showMessage(response.data.error_description);
            return Promise.resolve(false);
        });
    },
    userLoading: false,
    notificationsLoading: false,
    data: {},
    notifications: {},
    loggedIn: () => {
        return Request.loggedIn;
    },
    setLoggedIn: (value) => {
        Request.loggedIn = value;
    },
    queue: [],
    queueFn: (fn) => {
        if (User.data.hasOwnProperty('username')) {
            return fn();
        } else {
            User.queue.push(fn);
        }
    },
    getRankIcon: (type) => {
        switch(type) {
            case 'premium':
                return SVG.icon('fave');
            case 'hell-beta':
                return SVG.icon('flash');
            case 'beta':
                return SVG.icon('unfave');
            case 'volunteer':
                return SVG.icon('heart');
            case 'group':
                return SVG.icon('home');
            case 'super group':
                return SVG.icon('supergroup');
            case 'senior':
                return SVG.icon('ribbon');
            case 'admin':
                return SVG.icon('deviantart');
            default:
                return '';
        }
    },
    bannedClass: (type, selector = true) => {
        return type === 'banned' ? `${selector ? '.' : ' '}banned` : '';
    },
    matureFilter: () => {
        return User.mature ? '&mature_content=true' : '';
    },
    showMatureWarning: (deviation) => {
        return deviation && !User.mature;
    },
    toggleMature: () => {
        LocalStorage.set('mature', User.mature);
        Request.stopAllRequests();
        m.route.set(m.route.get());
    },
    mature: false
};

module.exports = User;
