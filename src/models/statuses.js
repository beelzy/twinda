const Entries = require('./entries.js');
const Request = require('../utils/request.js');

module.exports = new Entries.Instance({endpoint: Request.user, slug: 'statuses', name: 'status'});
