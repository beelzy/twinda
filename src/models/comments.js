const m = require('mithril');

const Request = require('../utils/request.js');
const Parser = require('../utils/parser.js');
const User = require('./user.js');

const Comments = {
    Instance: function(options = {}) {
        this.maxdepth = 5;
        this.main = {};
        this.replies = {};
        this.loading = false;
        this.reset = () => {
            this.main = {};
            this.replies = {};
        };
        this.parseComments = () => {
            for (let i = 0, size = this.main.thread.length; i < size; i++) {
                let comment = this.main.thread[i];
                comment.body = Parser.stripOutgoing(comment.body);
                this.main.thread[i] = comment;
            }
        };
        this.fetch = (options={}) => {
            this.loading = true;
            let offset = options.offset ? options.offset : 0;
            Request.abortRequests('comments');
            return Request.comments(`${options.slug}?limit=${options.pageSize}&offset=${offset}&maxdepth=${this.maxdepth}${User.matureFilter()}`, 'comments').then((response) => {
                this.main = response.data;
                this.parseComments();
                this.replies = {};
                if (this.main.hasOwnProperty('thread')) {
                    for(let i = this.main.thread.length - 1; i >= 0; i--) {
                        let comment = this.main.thread[i];
                        if (comment.parentid) {
                            this.replies[comment.parentid] = this.replies[comment.parentid] || [];
                            this.replies[comment.parentid].push(comment);
                            this.main.thread.splice(i, 1);
                        }
                    }
                    // The replies may end up on the last page, but without the parent comments so we try to grab them
                    if (this.main.thread.length === 0) {
                        let comments = [];
                        let parents = {};
                        let keys = Object.keys(this.replies);
                        for(let i = 0, size = keys.length; i < size; i++) {
                            let reply = this.replies[keys[i]];
                            for(let j = 0, repliesSize = reply.length; j < repliesSize; j++) {
                                comments.push(reply[j].commentid);
                            }
                            parents[keys[i]] = 1;
                        }

                        for(let i = 0, size = comments.length; i < size; i++) {
                            if (parents.hasOwnProperty(comments[i])) {
                                delete parents[comments[i]];
                            }
                        }

                        // There should really only be one...
                        keys = Object.keys(parents);
                        if (keys.length > 0) {
                            Request.comments(`${keys[0]}/siblings`, 'comments').then((response) => {
                                this.main = response.data;
                                this.parseComments();
                                m.redraw();
                            });
                        }
                    }
                }
                m.redraw();
                return response;
            }).finally(() => {
                this.loading = false;
            });
        }
    }
};

module.exports = Comments;
