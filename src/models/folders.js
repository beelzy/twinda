const m = require('mithril');
const Request = require('../utils/request.js');
const User = require('./user.js');

const Folders = {
    Instance: function(options = {}) {
        this.size = options.size ? options.size : 10;
        this.flat = options.flat ? options.flat : false;
        this.index = 0;
        this.hasMore = true;
        this.data = [];
        this.parentid = '';
        this.folderid = '';
        this.rootid = '';
        this.name = '';
        this.display = [];
        this.loading = false;
        this.error = false;
        this.error_description = '';
        this.reset = () => {
            this.index = 0;
            this.hasMore = true;
            this.data = [];
            this.display = [];
            this.parentid = '';
            this.folderid = '';
            this.rootid = '';
            this.name = '';
            this.error = false;
            this.error_description = '';
        };
        this.filterDisplayFolders = (options = {}) => {
            if (options.reset) {
                this.display = [];
            }
            let data = options.data ? options.data : this.data;
            for(let i = 0, size = data.length; i < size; i++) {
                let folder = data[i];
                let id = this.folderid === 'all' ? this.rootid : this.folderid;
                let addFolder = false;
                if (this.flat) {
                    if (folder.name !== 'Featured') {
                        addFolder = true;
                    }
                } else {
                    if (folder.parent === id) {
                        addFolder = true;
                    }
                }
                if (addFolder) {
                    this.display.push(folder);
                }
            } 
        };
        this.findParent = () => {
            if (this.flat) {
                this.parentid = this.folderid !== this.rootid && this.folderid !== 'all' ? this.rootid : '';
            }

            for(let i = 0, size = this.data.length; i < size; i++) {
                let folder = this.data[i];
                if (folder.folderid === this.folderid) {
                    if (!this.flat) {
                        this.parentid = folder.parent;
                    }
                    this.name = folder.name;
                }
            }
        };
        this.normalizeAllFolder = () => {
            if (this.folderid === 'all' && this.flat) {
                if (this.data.length > 0) {
                    this.folderid = this.rootid === ''
                        ? this.data[0].folderid : this.rootid;
                }
                this.name = 'Featured';
            }
        };
        this.fetch = (options = {}) => {
            this.loading = true;
            this.normalizeAllFolder();
            return options.endpoint(`${options.slug}${options.query}&ext_preload=1&limit=${this.size}&offset=${this.index}`).then((response) => {
                if (response.data.hasOwnProperty('error')) {
                    this.error = true;
                    this.error_description = response.data.error_description;
                    this.loading = false;
                    return Promise.resolve(false);
                }
                for(let i = 0, size = response.data.results.length; i < size; i++) {
                    let folder = response.data.results[i];
                    if (folder.folderid === this.folderid) {
                        this.parentid = this.flat ? this.parentid : folder.parent;
                        this.name = folder.name;
                    }
                    if (this.flat && folder.name === 'Featured') {
                        this.rootid = folder.folderid;
                        if (this.folderid !== 'all' && this.folderid !== this.rootid) {
                            this.parentid = folder.folderid;
                        }
                        this.filterDisplayFolders({reset: true});
                    }
                    if (folder.parent === null && !this.flat) {
                        this.rootid = folder.folderid;
                        if (this.folderid === 'all') {
                            this.filterDisplayFolders({reset: true});
                        }
                    }
                }
                this.data = [...this.data, ...response.data.results];
                this.hasMore = response.data.has_more;
                this.index = response.data.next_offset;

                this.filterDisplayFolders({data: response.data.results});

                m.redraw();

                if (this.hasMore) {
                    this.fetch(options);
                    return Promise.resolve(false);
                } else {
                    this.loading = false;
                    return Promise.resolve(true);
                }
            });
        };
    }
};

module.exports = Folders;
