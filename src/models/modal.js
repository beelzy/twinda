const m = require('mithril');

const RequestAnimation = require('../utils/request-animation.js');

const Modal = {
    messages: [],
    showMessage: (message) => {
        Modal.messages.push(message);
        RequestAnimation.requestTimeout(() => {
            Modal.messages.shift();
            m.redraw();
        }, 3000);
    }
};

module.exports = Modal;
