const Request = require('../utils/request.js');

const Collections = {
    hasMore: true,
    data: [],
    selected: [],
    offset: 0,
    favesCommitted: true,
    unfavesCommitted: true,
    user: '',
    clear: () => {
        Collections.offset = 0;
        Collections.data = [];
        Collections.selected = [];
        Collections.user = '';
        Collections.hasMore = true;
        Collections.favesCommitted = true;
        Collections.unfavesCommitted = true;
    },
    commit: (faves = true) => {
        for (let i = 0, size = Collections.selected.length; i < size; i++) {
            if (Collections.selected[i] === faves) {
                Collections.data[i].selected = Collections.selected[i];
            }
        }
    },
    reset: () => {
        for (let i = 0, size = Collections.selected.length; i < size; i++) {
            Collections.selected[i] = Collections.data[i].selected;
        }
    },
    checkSelected: (options = {}) => {
        if (options.reset) {
            Collections.selected = [];
        }
        let data = options.data ? options.data : Collections.data;
        for(let i = 0, size = data.length; i < size; i++) {
            let collection = data[i];
            collection.selected = false;
            for(let j = 0, collectionSize = options.metadata.length; j < collectionSize; j++) {
                if (collection.folderid === options.metadata[j].folderid) {
                    collection.selected = true;
                    break;
                }
            }
            data[i] = collection;
            Collections.selected.push(collection.selected);
        }
    },
    fetch: (options={}) => {
        return Request.collections(`folders?username=${options.user}&offset=${Collections.offset}`).then((response) => {
            if (response.data.hasOwnProperty('results')) {
                Collections.checkSelected({data: response.data.results, metadata: options.metadata});
                Collections.offset += 10;
                Collections.hasMore = response.data.has_more;
                Collections.data = [...Collections.data, ...response.data.results];
            } else {
                Collections.data = response.data;
            }
        })
    }
};

module.exports = Collections;
