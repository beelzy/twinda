const m = require('mithril');

const Request = require('../utils/request.js');
const Parser = require('../utils/parser.js');
const User = require('./user.js');
const Pagination = require('./pagination.js');
const Tabs = require('./tabs.js');

const Entries = {
    Instance: function(options = {}) {
        this.data = {};
        this.page = new Pagination.Instance();
        this.profile = {};
        this.username = '';
        this.featured = true;
        this.loading = false;
        this.selectedMenuItem = '';
        this.name = options.name ? options.name : 'journal';
        this.endpoint = options.endpoint ? options.endpoint : Request.browse;
        this.slug = options.slug ? options.slug : 'user/journals';
        this.init = (user) => {
            this.profile = {};
            if (user !== this.username) {
                Tabs.reset();
            }
            this.username = user;
            Tabs.username = user;
            this.featured = true;
            this.page.reset();
            this.selectedMenuItem = '';
            return Tabs.fetchProfile();
        };
        this.fetch = (options={}) => {
            this.loading = true;
            Request.abortRequests(this.name);
            let offset = options.offset ? options.offset : this.page.index * this.page.size;
            return this.endpoint(`${this.slug}/?username=${this.username}&limit=${this.page.size}&offset=${offset}&featured=${this.featured ? 1 : 0}${User.matureFilter()}`, this.name).then((response) => {
                this.data = response.data;
                if (!response.data.has_more) {
                    this.page.total = Math.min(this.page.total, this.page.index + 1);
                }
                if (this.data.hasOwnProperty('results')) {
                    for (let i = 0, size = this.data.results.length; i < size; i++) {
                        let entry = this.data.results[i];
                        let property = this.name === 'journal' ? 'excerpt' : 'body';
                        let content = entry[property];
                        content = Parser.parseAll(content);
                        entry[property] = content;
                        this.data.results[i] = entry;
                    }
                }

                m.redraw();
            }).finally(() => {
                this.loading = false;
            });
        };
        this.changePage = (page) => {
            this.page.index = page;
            this.fetch();
        };
        this.toggleFeatured = (value) => {
            this.featured = value;
            this.page.reset();
            return this.fetch();
        };
        this.selectMenuItem = (item) => {
            this.selectedMenuItem = this.selectedMenuItem === item ? '' : item;
        };
    }
};

module.exports = Entries;
