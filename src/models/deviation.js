const m = require('mithril');

const Request = require('../utils/request.js');
const Locale = require('../utils/locale.js');
const Parser = require('../utils/parser.js');
const User = require('./user.js');
const Comments = require('./comments.js');
const Pagination = require('./pagination.js');
const Collections = require('./user-collections.js');
const Users = require('./users.js');
const Modal = require('./modal.js');

const Deviation = {
    init: (id) => {
        Deviation.id = id;
        Deviation.page.reset();
        Deviation.comments.reset();
        Deviation.related = {};
        Deviation.metadata = {};
        Deviation.content = {};
        Deviation.favorites.reset();
        Deviation.showDetails = false;
        Deviation.showCollection = false;
        Deviation.showFullView = false;
        Deviation.commentEdit = '';
        Deviation.page.total = Math.ceil(10000 / Deviation.page.size);
        return Request.deviation(id).then((response) => {
            Deviation.data = response.data;
            document.title = `${Locale.title} - Deviation: ${Deviation.data.title}`;
            Deviation.fetchMetadata().then(() => {
                m.redraw();
            });

            if (!Deviation.hasOwnProperty('error')) {
                if (!Deviation.data.hasOwnProperty('preview') && !User.showMatureWarning(Deviation.data.is_mature)) {
                    Request.deviation(`content?deviationid=${Deviation.id}`).then((response) => {
                        Deviation.content = response.data;
                        Deviation.content.html = Parser.parseAll(Deviation.content.html);
                        m.redraw();
                    });
                }

                if (User.loggedIn()) {
                    User.queueFn(Deviation.fetchWatching);
                }
            }
            m.redraw();
        });
    },
    id: '',
    fetchWatching: () => {
        User.isWatching(Deviation.data.author.username, Deviation.data.author.userid).then((response) => {
            Deviation.watching = response;
            Deviation.watchStatus = true;
            m.redraw();
        });
    },
    fetchMetadata: () => {
        return Request.deviation(`metadata?deviationids[0]=${Deviation.id}&ext_submission=1&ext_stats=1${User.loggedIn() ? '&ext_collection=1' : ''}`).then((response) => {
            if (response.data.hasOwnProperty('metadata')) {
                Deviation.metadata = response.data.metadata[0];
                Deviation.metadata.description = Parser.parseAll(Deviation.metadata.description);
                Collections.checkSelected({reset: true, metadata: Deviation.metadata.collections});
            } else {
                Deviation.metadata = response.data;
            }
        });
    },
    fetchComments: (options={}) => {
        let offset = options.offset ? options.offset : Deviation.page.index * Deviation.page.size;
        return Deviation.comments.fetch({slug: `deviation/${Deviation.id}`, pageSize: Deviation.page.size, offset: offset}).then((response) => {
            if (!response.data.hasOwnProperty('error')) {
                Deviation.page.setTotal(response.data.total);
            }
        });
    },
    fetchRelated: () => {
        return Request.browse(`morelikethis/preview?seed=${Deviation.id}`).then((response) => {
            Deviation.related = response.data;
            m.redraw();
        });
    },
    fetchDownload: () => {
        return Request.deviation(`download/${Deviation.id}`).then((response) => {
            if (response.hasOwnProperty('data')) {
                window.open(response.data.src);
            }
        });
    },
    fetchCollections: () => {
        Deviation.loadingCollections = true;
        Collections.id = Deviation.id;
        return Collections.fetch({user: User.data.username, metadata: Deviation.metadata.collections}).finally(() => {
            Deviation.loadingCollections = false;
            m.redraw();
        });
    },
    fetchFaves: () => {
        return Deviation.favorites.fetch({
            endpoint: Request.deviation,
            slug: 'whofaved',
            query: `?deviationid=${Deviation.id}`
        });
    },
    postFave: (unfave = false, options = {}) => {
        Deviation.postingFave = true;
        let content = {deviationid: Deviation.id};
        if (options.hasOwnProperty('folders')) {
            for(let i = 0, size = options.folders.length; i < size; i++) {
                content[`folderid[${i}]`] = options.folders[i];
            }
        }
        return Request.collectionsPost(unfave ? 'unfave' : 'fave', content).then((response) => {
            if (response.data.success) {
                Deviation.fetchMetadata().then(() => {
                    m.redraw();
                }).finally(() => {
                    Deviation.postingFave = false;
                });
            } else {
                Modal.showMessage(`Could not ${unfave ? 'unfave' : 'fave'}: ${response.data.error_description}`);
            }
            m.redraw();
        });
    },
    postCollections: () => {
        Deviation.postingFave = true;
        Collections.favesCommitted = false;
        Collections.unfavesCommitted = false;
        let faves = [];
        let unfaves = [];
        for(let i = 0, size = Collections.selected.length; i < size; i++) {
            if (Collections.selected[i] !== Collections.data[i].selected) {
                if (Collections.selected[i]) {
                    faves.push(Collections.data[i].folderid);
                } else {
                    unfaves.push(Collections.data[i].folderid);
                }
            }
        }
        if (faves.length > 0) {
            Deviation.postFave(false, {folders: faves}).then(() => {
                Collections.commit(true);
                m.redraw();
            }).finally(() => {
                Collections.favesCommitted = true;
                Deviation.postingFave = !Collections.unfavesCommitted;
            });
        } else {
            Collections.favesCommitted = true;
        }
        if (unfaves.length > 0) {
            Deviation.postFave(true, {folders: unfaves}).then(() => {
                Collections.commit(false);
                m.redraw();
            }).finally(() => {
                Collections.unfavesCommitted = true;
                Deviation.postingFave = !Collections.favesCommitted;
            });
        } else {
            Collections.unfavesCommitted = true;
        }
    },
    postWatch: (unwatch = false) => {
        Deviation.postingWatch = true;
        return User.postWatch({unwatch: unwatch, user: Deviation.data.author.username}).then((response) => {
            if (response) {
                Deviation.watching = !unwatch;
            }
            m.redraw();
        }).finally(() => {
            Deviation.postingWatch = false;
        });
    },
    postComment: (options={}) => {
        Deviation.postingComment = true;
        let body = options.body ? options.body : Deviation.commentEdit;
        let content = {body: body};
        if (options.commentid) {
            content.commentid = options.commentid;
        }
        Request.abortRequests('comments');
        return Request.commentsPost(`post/deviation/${Deviation.id}`, content, 'comments').then((response) => {
            Deviation.commentEdit = '';
            if (response.data.hasOwnProperty('commentid') && response.data.commentid !== '') {
                Deviation.metadata.stats.comments += 1;
                Deviation.postedCommentId = response.data.commentid;
            }
            if (response.data.hasOwnProperty('error')) {
                Modal.showMessage(response.data.error_description);
            }
            return Deviation.fetchComments();
        }).finally(() => {
            Deviation.postingComment = false;
        });
    },
    changePage: (page) => {
        Deviation.page.index = page;
        Deviation.fetchComments();
    },
    extractEmbedID: (url) => {
        let parts = url.split('-');
        return parts[parts.length - 1];
    },
    setCommentEdit: (comment) => {
        Deviation.commentEdit = comment;
    },
    data: {},
    metadata: {},
    content: {},
    comments: new Comments.Instance(),
    page: new Pagination.Instance(),
    related: {},
    commentEdit: '',
    postingComment: false,
    postedCommentId: '',
    postingFave: false,
    postingWatch: false,
    watching: false,
    shareText: 'Shared through twinDA.',
    showDetails: false,
    showCollection: false,
    showFullView: false,
    loadingCollections: false,
    favorites: new Users.Instance(),
    watchStatus: false
};

module.exports = Deviation;
