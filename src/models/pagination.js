const Pagination = {
    // This needs to be ES5 function because new Instance doesn't work with ES6
    Instance: function(options = {}) {
        this.size = 20;
        this.total = Math.ceil(50000 / this.size);
        this.cache = 5;
        this.cacheMobile = 3;
        this.index = 0;
        this.reset = () => {
            this.index = 0;
            this.total = Math.ceil(50000 / this.size);
        };
        this.setTotal = (newTotal) => {
            this.total = Math.min(this.total, Math.ceil(newTotal / this.size));
        };
    }
};

module.exports = Pagination;
