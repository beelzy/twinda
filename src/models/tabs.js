const m = require('mithril');

const Request = require('../utils/request.js');
const User = require('./user.js');

const Tabs = {
    data: {},
    isWatching: false,
    watchStatus: false,
    postingWatch: false,
    username: '',
    postWatch: (unwatch = false) => {
        Tabs.postingWatch = true;
        return User.postWatch({unwatch: unwatch, user: Tabs.username}).then((response) => {
            if (response) {
                Tabs.isWatching = !unwatch;
            }
            m.redraw();
        }).finally(() => {
            Tabs.postingWatch = false;
        });
    },
    reset: () => {
        Tabs.isWatching = false;
        Tabs.watchStatus = false;
    },
    isSameUser: () => {
        return User.data.username && Tabs.username && User.data.username.toLowerCase() === Tabs.username.toLowerCase();
    },
    fetchProfile: (extended=false) => {
        Tabs.data = {};
        return Request.user(`profile/${Tabs.username}${extended ? '?ext_galleries=1&ext_collections=1' : ''}`).then((response) => {
            Tabs.data = response.data;
            m.redraw();

            if (User.loggedIn() && !Tabs.isSameUser() && !Tabs.data.hasOwnProperty('error')) {
                User.queueFn(Tabs.fetchWatching);
            }
            return Promise.resolve(response);
        });
    },
    fetchWatching: () => {
        if (Tabs.data.hasOwnProperty('user')) {
            return User.isWatching(Tabs.data.user.username, Tabs.data.user.userid).then((response) => {
                Tabs.isWatching = response;
                Tabs.watchStatus = true;
                m.redraw();
            });
        }
    }
};

module.exports = Tabs;
