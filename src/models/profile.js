const m = require('mithril');

const Request = require('../utils/request.js');
const User = require('./user.js');
const Comments = require('./comments.js');
const Pagination = require('./pagination.js');
const Users = require('./users.js');
const Modal = require('./modal.js');
const Tabs = require('./tabs.js');
const Parser = require('../utils/parser.js');

const Profile = {
    init: (username) => {
        Profile.data = {};
        Profile.page.reset();
        Profile.comments.reset();
        Profile.username = username;
        Tabs.username = username;
        Profile.featured = {};
        Profile.featuredid = '';
        Profile.collection = {};
        Profile.collectionid = '';
        Profile.statuses = {};
        Profile.watching.reset();
        Profile.watchers.reset();
        Profile.hasFeatured = true;
        Profile.commentEdit = '';
        Tabs.reset();
        return Tabs.fetchProfile(true).then((response) => {
            Profile.data = response.data;
            if (Profile.data.hasOwnProperty('galleries')) {
                for(let i = 0, size = Profile.data.galleries.length; i < size; i++) {
                    if (Profile.data.galleries[i].parent === null) {
                        Profile.featuredid = Profile.data.galleries[i].folderid;
                        break;
                    }
                }
            }
            if (Profile.data.hasOwnProperty('collections')) {
                for(let i = 0, size = Profile.data.collections.length; i < size; i++) {
                    if (Profile.data.collections[i].name === 'Featured') {
                        Profile.collectionid = Profile.data.collections[i].folderid;
                        break;
                    }
                }
            }
            if (Profile.data.bio) {
                Profile.data.bio = Parser.parseAll(Profile.data.bio);
            }

            Profile.fetchFeatured().then(() => {
                m.redraw();
            });

            Profile.fetchCollection().then(() => {
                m.redraw();
            });
            m.redraw();
        });
    },
    fetchFeatured: () => {
        return Request.gallery(`${Profile.featuredid}?username=${Profile.username}&limit=1${User.matureFilter()}`).then((response) => {
            if (response.data.hasOwnProperty('results') && response.data.results.length > 0) {
                Profile.featured = response.data.results[0];
                m.redraw();
            } else {
                Profile.hasFeatured = false;
            }
        });
    },
    fetchStatuses: () => {
        return Request.user(`statuses?username=${Profile.username}${User.matureFilter()}`).then((response) => {
            Profile.statuses = response.data;

            if (Profile.statuses.hasOwnProperty('results')) {
                for (let i = 0, size = Profile.statuses.results.length; i < size; i++) {
                    let status = Profile.statuses.results[i];
                    status.body = Parser.parseAll(status.body);
                    Profile.statuses.results[i] = status;
                }
            }

            m.redraw();
        });
    },
    fetchCollection: () => {
        return Request.collections(`${Profile.collectionid}?username=${Profile.username}${User.matureFilter()}`).then((response) => {
            Profile.collection = response.data;
        });
    },
    fetchWatching: () => {
        return Profile.watching.fetch({slug: `friends/${Profile.username}`});
    },
    fetchWatchers: () => {
        return Profile.watchers.fetch({slug: `watchers/${Profile.username}`});
    },
    fetchComments: (options={}) => {
        let offset = options.offset ? options.offset : Profile.page.index * Profile.page.size;
        return Profile.comments.fetch({slug: `profile/${Profile.username}`, pageSize: Profile.page.size, offset: offset}).then((response) => {
            Profile.page.setTotal(response.data.total);
        });
    },
    changePage: (page) => {
        Profile.page.index = page;
        Profile.fetchComments();
    },
    postWatch: (unwatch = false) => {
        return User.postWatch({unwatch: unwatch, user: Profile.username}).then((response) => {
            if (response) {
                Profile.isWatching = !unwatch;
            }
        });
    },
    postComment: (options={}) => {
        Profile.postingComment = true;
        let body = options.body ? options.body : Profile.commentEdit;
        let content = {body: body};
        if (options.commentid) {
            content.commentid = options.commentid;
        }
        Request.abortRequests('comments');
        return Request.commentsPost(`post/profile/${Profile.username}`, content, 'comments').then((response) => {
            Profile.commentEdit = '';
            if (response.data.hasOwnProperty('commentid') && response.data.commentid !== '') {
                Profile.data.stats.profile_comments += 1;
                Profile.postedCommentId = response.data.commentid;
            }
            if (response.data.hasOwnProperty('error')) {
                Modal.showMessage(response.data.error_description);
            }
            return Profile.fetchComments();
        }).finally(() => {
            Profile.postingComment = false;
        });
    },
    setCommentEdit: (comment) => {
        Profile.commentEdit = comment;
    },
    username: '',
    data: {},
    featuredid: '',
    featured: {},
    hasFeatured: true,
    statuses: {},
    collectionid: '',
    collection: {},
    watching: new Users.Instance(),
    watchers: new Users.Instance(),
    comments: new Comments.Instance(),
    page: new Pagination.Instance(),
    commentEdit: '',
    postingComment: false,
    postedCommentId: ''
};

module.exports = Profile;
