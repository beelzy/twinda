const m = require('mithril');

const Request = require('../utils/request.js');
const Locale = require('../utils/locale.js');
const Parser = require('../utils/parser.js');
const DateFormatter = require('../utils/date.js');
const Comments = require('./comments.js');
const Pagination = require('./pagination.js');
const Tabs = require('./tabs.js');
const Modal = require('./modal.js');

const Status = {
    init: (id) => {
        Status.id = id;
        Status.data = {};
        Status.comments.reset();
        Status.page.reset();
        Status.loading = true;
        return Request.user(`statuses/${Status.id}`).then((response) => {
            Status.data = response.data;
            if (Status.data.hasOwnProperty('author')) {
                document.title = `${Locale.title} - ${Status.data.author.username} Status: ${DateFormatter.formatFromString(Status.data.ts)}`;
                Status.data.body = Parser.parseAll(Status.data.body);
            }
            Tabs.username = Status.data.author.username;

            Tabs.fetchProfile();

            m.redraw();
        }).finally(() => {
            Status.loading = false;
        });
    },
    fetchComments: (options={}) => {
        let offset = options.offset ? options.offset : Status.page.index * Status.page.size;
        return Status.comments.fetch({slug: `status/${Status.id}`, pageSize: Status.page.size, offset: offset}).then((response) => {
            if (!response.data.hasOwnProperty('error')) {
                Status.page.setTotal(response.data.total);
            }
        });
    },
    postComment: (options={}) => {
        Status.postingComment = true;
        let body = options.body ? options.body : Status.commentEdit;
        let content = {body: body};
        if (options.commentid) {
            content.commentid = options.commentid;
        }
        Request.abortRequests('comments');
        return Request.commentsPost(`post/status/${Status.id}`, content, 'comments').then((response) => {
            Status.commentEdit = '';
            if (response.data.hasOwnProperty('error')) {
                Modal.showMessage(response.data.error_description);
            } else {
                Status.postedCommentId = response.data.commentid;
            }
            return Status.fetchComments();
        }).finally(() => {
            Status.postingComment = false;
        });
    },
    changePage: (page) => {
        Status.page.index = page;
        Status.fetchComments();
    },
    setCommentEdit: (comment) => {
        Status.commentEdit = comment;
    },
    id: '',
    data: {},
    postingComment: false,
    commentEdit: '',
    comments: new Comments.Instance(),
    page: new Pagination.Instance(),
    postedCommentId: ''
};

module.exports = Status;
