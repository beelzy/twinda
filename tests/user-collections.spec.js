const o = require('ospec');

const Collections = require('../src/models/user-collections.js');
const Mock = require('./mock.js');

o.spec('User Collections Model', () => {
    o.specTimeout(10000);
    o('Check selected folders', () => {
        Collections.data = Mock().usercollections.data;
        Collections.checkSelected({metadata: Mock().usercollections.selected});
        o(Collections.selected.length).equals(3);
        o(Collections.selected[0]).equals(true);
        o(Collections.selected[1]).equals(false);
        o(Collections.selected[2]).equals(false);
    });
    o('Reset selected folders', () => {
        Collections.data = Mock().usercollections.data;
        Collections.reset();
        o(Collections.selected[0]).equals(false);
        o(Collections.selected[1]).equals(true);
        o(Collections.selected[2]).equals(true);
    });
    o('Commit selected folders', () => {
        Collections.data = Mock().usercollections.data;
        Collections.selected = [true, false, false];
        Collections.commit();
        o(Collections.data[0].selected).equals(true);
        o(Collections.data[1].selected).equals(true);
        o(Collections.data[2].selected).equals(true);
        Collections.commit(false);
        o(Collections.data[0].selected).equals(true);
        o(Collections.data[1].selected).equals(false);
        o(Collections.data[2].selected).equals(false);
    });
});
