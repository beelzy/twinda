const o = require('ospec');

const Profile = require('../src/models/profile.js');

o.spec('Profile Model', () => {
    o.specTimeout(10000);
    o('Initialize class', (done) => {
        Profile.init('Xelku9').then(() => {
            o(Profile.featuredid).equals('9FC93140-97CD-3092-4C03-187D6F215296');
            o(Profile.collectionid).equals('59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C');

            done();
        });
    });
    o('Fetch featured deviation', (done) => {
        Profile.featuredid = '9FC93140-97CD-3092-4C03-187D6F215296';
        Profile.username = 'Xelku9';
        Profile.fetchFeatured().then(() => {
            o(Profile.featured.deviationid).equals('5365EC7C-EBFB-DED7-8CAA-014C9EB14DC4');
            o(Profile.hasFeatured).equals(true);

            done();
        });
    });
    o('Fetch statuses', (done) => {
        Profile.username = 'spyed';
        Profile.fetchStatuses().then(() => {
            o(Profile.statuses.results.length > 0).equals(true);

            done();
        });
    });
});
