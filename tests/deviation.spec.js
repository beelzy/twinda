const o = require('ospec');

const Deviation = require('../src/models/deviation.js');

o.spec('Deviation Model', () => {
    o.specTimeout(10000);
    o('Initialize class', (done) => {
        Deviation.init('847AA897-5D7D-1115-0D68-537F60049873').then(() => {
            o(Deviation.data.hasOwnProperty('thumbs')).equals(true);

            done();
        });
        o(Deviation.id).equals('847AA897-5D7D-1115-0D68-537F60049873');
        o(Object.keys(Deviation.related).length).equals(0);
        o(Object.keys(Deviation.metadata).length).equals(0);
        o(Object.keys(Deviation.content).length).equals(0);
        o(Deviation.showDetails).equals(false);
        o(Deviation.showCollection).equals(false);
        o(Deviation.showFullView).equals(false);
        o(Deviation.commentEdit).equals('');
    });
    o('Fetch metadata', (done) => {
        Deviation.fetchMetadata().then(() => {
            o(Deviation.metadata.hasOwnProperty('stats')).equals(true);

            done();
        });
    });
    o('Fetch related deviations', (done) => {
        Deviation.id = '847AA897-5D7D-1115-0D68-537F60049873';
        Deviation.fetchRelated().then(() => {
            o(Deviation.related.hasOwnProperty('author')).equals(true);
            o(Deviation.related.author.userid).equals('798370A7-F037-7F2D-B440-F2635998E3EF');

            done();
        });
    });
    o('Extract Embed ID', () => {
        o(Deviation.extractEmbedID('https://www.deviantart.com/xelku9/art/Bring-Back-Classic-Da-842944122')).equals('842944122');
    });
});
