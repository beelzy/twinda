const o = require('ospec');

const User = require('../src/models/user.js');
const Mock = require('./mock.js')();

o.spec('User API Calls', () => {
    o.specTimeout(10000);
    o('Fetch user', (done) => {
        let functionCalled = false;
        User.queueFn(() => {
            functionCalled = true;
        });
        o(functionCalled).equals(false);
        User.fetchUser().then((response) => {
            o(User.queue.length).equals(0);
            o(User.userLoading).equals(false);
            o(User.loggedIn()).equals(false);
            o(functionCalled).equals(true);

            done();
        });
        o(User.userLoading).equals(true);
    });
    o('initialize class', (done) => {
        User.init().then(() => {
            o(User.notificationsLoading).equals(false);
            done();
        });
        o(User.notificationsLoading).equals(true);
    });
});
