const o = require('ospec');

const Gallery = require('../src/models/gallery.js');
const Collections = require('../src/models/collections.js');

o.spec('Gallery Model', () => {
    o.specTimeout(10000);
    o('Fetch gallery', (done) =>  {
        Gallery.username = 'Xelku9';
        Gallery.fetchGallery().then(() => {
            o(Gallery.loading).equals(false);
            o(Gallery.data.hasOwnProperty('error')).equals(false);

            done();
        });
        o(Gallery.loading).equals(true);
    });
    o('Fetch collections', (done) =>  {
        Collections.username = 'Xelku9';
        Collections.id = '59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C';
        Collections.folders.folderid = '59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C';
        Collections.fetchGallery().then(() => {
            o(Collections.loading).equals(false);
            o(Collections.data.hasOwnProperty('error')).equals(false);

            done();
        });
        o(Collections.loading).equals(true);
    });
});
