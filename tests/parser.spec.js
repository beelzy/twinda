const o = require('ospec');

const Parser = require('../src/utils/parser.js');
const Mock = require('./mock.js')();

o.spec('Parser Functions', () => {
    o('Strip script tags', () => {
        o(Mock.parser.scripts.includes('<script')).equals(true);
        o(Mock.parser.scripts.includes('</script>')).equals(true);
        let output = Parser.stripScripts(Mock.parser.scripts);
        o(output.includes('<script')).equals(false);
        o(output.includes('</script>')).equals(false);
    });
    o('Replace youtube with youtube-nocookie', () => {
        o(Mock.parser.youtube.includes('<iframe')).equals(true);
        o(Mock.parser.youtube.includes('https://www.youtube.com/embed/')).equals(true);
        let output = Parser.nocookieYoutube(Mock.parser.youtube);
        o(output.includes('https://www.youtube-nocookie.com/embed/')).equals(true);
        o(output.includes('https://www.youtube.com/embed/')).equals(false);
    });
    o('Strip outgoing link redirects', () => {
        o(Mock.parser.external.includes('deviantart.com/users/outgoing?')).equals(true);
        let output = Parser.stripOutgoing(Mock.parser.external);
        o(output.includes('deviantart.com/users/outgoing?')).equals(false);
    });
});
