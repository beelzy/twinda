const o = require('ospec');

const Tabs = require('../src/models/tabs.js');

o.spec('Tabs Model', () => {
    o.specTimeout(10000);
    o('Fetch profile', (done) => {
        Tabs.username = 'Xelku9';
        Tabs.fetchProfile().then((response) => {
            o(response.data.hasOwnProperty('galleries')).equals(false);
            o(response.data.hasOwnProperty('collections')).equals(false);

            done();
        });
    });
    o('Fetch profile (extended)', (done) => {
        Tabs.username = 'Xelku9';
        Tabs.fetchProfile(true).then((response) => {
            o(response.data.hasOwnProperty('galleries')).equals(true);
            o(response.data.hasOwnProperty('collections')).equals(true);

            done();
        });
    });
});
