const o = require('ospec');

const Comments = require('../src/models/comments.js');

o.spec('Comments Class', () => {
    o.specTimeout(10000);
    o('Fetch comments', (done) => {
        let comments = new Comments.Instance();

        comments.fetch({slug: 'deviation/847AA897-5D7D-1115-0D68-537F60049873'}).then((response) => {
            o(comments.loading).equals(false);
            o(comments.main.hasOwnProperty('thread')).equals(true);

            comments.reset();

            o(comments.main.hasOwnProperty('thread')).equals(false);
            o(Object.keys(comments.replies).length).equals(0);

            done();
        });
        o(comments.loading).equals(true);
    });
});
