const o = require('ospec');

const Entries = require('../src/models/entries.js');
const Request = require('../src/utils/request.js');

o.spec('Entries Class', () => {
    o.specTimeout(10000);
    o('Fetch journal entries', (done) => {
        let entries = new Entries.Instance();
        entries.username = 'Xelku9';
        entries.fetch().then(() => {
            o(entries.data.hasOwnProperty('results')).equals(true);
            o(entries.loading).equals(false);

            done();
        });
        o(entries.loading).equals(true);
    });
    o('Fetch status entries', (done) => {
        let entries = new Entries.Instance({endpoint: Request.user, slug: 'statuses', name: 'status'});
        entries.username = 'JenFruzz';
        entries.fetch().then(() => {
            o(entries.data.hasOwnProperty('results')).equals(true);
            o(entries.loading).equals(false);

            done();
        });
        o(entries.loading).equals(true);
    });
});
