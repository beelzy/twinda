const o = require('ospec');

const Folders = require('../src/models/folders.js');
const Request = require('../src/utils/request.js');

o.spec('Folders Class', () => {
    o.specTimeout(10000);
    o('Fetch folders', (done) => {
        let folders = new Folders.Instance();

        o(folders.flat).equals(false);
        folders.folderid = '640E5137-3390-4BD4-2D7A-3426C2F1398D';
        folders.fetch({slug: 'folders', endpoint: Request.gallery, query: '?username=Xelku9'}).then(() => {
            o(folders.parentid).equals('9FC93140-97CD-3092-4C03-187D6F215296');
            o(folders.rootid).equals('9FC93140-97CD-3092-4C03-187D6F215296');
            o(folders.name).equals('Darling in the FRANXX');

            done();
        });
        o(folders.loading).equals(true);
    });
    o('Find parent', (done) => {
        let folders = new Folders.Instance();
        folders.folderid = '9FC93140-97CD-3092-4C03-187D6F215296';
        folders.fetch({slug: 'folders', endpoint: Request.gallery, query: '?username=Xelku9'}).then(() => {
            o(folders.parentid).equals(null);
            o(folders.name).equals('Featured');
            folders.folderid = '640E5137-3390-4BD4-2D7A-3426C2F1398D';
            folders.findParent();
            o(folders.parentid).equals('9FC93140-97CD-3092-4C03-187D6F215296');
            o(folders.name).equals('Darling in the FRANXX');

            done();
        });
    })
    o('Filter display folders', (done) => {
        let folders = new Folders.Instance();
        folders.folderid = 'all';
        folders.fetch({slug: 'folders', endpoint: Request.gallery, query: '?username=Xelku9'}).then(() => {
            o(folders.name).equals('');
            o(folders.display.length > 0).equals(true);

            folders.filterDisplayFolders({reset: true});

            o(folders.display.length > 0).equals(true);

            done();
        });
    });
    o('Fetch folders (flat)', (done) => {
        let folders = new Folders.Instance({flat: true});

        o(folders.flat).equals(true);
        folders.folderid = '1DAC2645-0925-1B8A-FB8B-EC53ABA9E9F2';
        folders.fetch({slug: 'folders', endpoint: Request.collections, query: '?username=Xelku9'}).then(() => {
            o(folders.parentid).equals('59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C');
            o(folders.rootid).equals('59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C');
            o(folders.name).equals('Midna');

            done();
        });
        o(folders.loading).equals(true);
    });
    o('Find parent (flat)', (done) => {
        let folders = new Folders.Instance({flat: true});
        folders.folderid = '59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C';
        folders.fetch({slug: 'folders', endpoint: Request.collections, query: '?username=Xelku9'}).then(() => {
            o(folders.parentid).equals('');
            o(folders.name).equals('Featured');
            folders.folderid = '7B276A5B-66C7-9DA2-9B69-44A7F1FD6766';
            folders.findParent();
            o(folders.parentid).equals('59A69EA3-5D5A-0D1B-6A78-8BB005E2B58C');
            o(folders.name).equals('Avatar The Legend of Korra');

            done();
        });
    })
    o('Filter display folders (flat)', (done) => {
        let folders = new Folders.Instance({flat: true});
        folders.folderid = 'all';
        folders.fetch({slug: 'folders', endpoint: Request.collections, query: '?username=Xelku9'}).then(() => {
            o(folders.name).equals('Featured');
            o(folders.display.length > 0).equals(true);

            folders.filterDisplayFolders({reset: true});

            o(folders.display.length > 0).equals(true);

            done();
        });
    });
});
