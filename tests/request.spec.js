const o = require('ospec');

const Request = require('../src/utils/request.js');

o.spec('Request DeviantArt API', () => {
    o.specTimeout(10000);
    o('API Get call', (done) => {
        Request.apiGet('browse/newest').then((response) => {
            o(response.data.hasOwnProperty('results')).equals(true);
            o(response.data.results.length > 0).equals(true);

            done();
        });
    });
    o('Unauthorized API call', (done) => {
        o(Request.loggedIn).equals(false);
        Request.apiGet('feed/notifications').then((response) => {
            o(response.data.hasOwnProperty('error')).equals(true);

            done();
        });
    });
    o('stop API calls', () => {
        Request.apiGet('browse/newest');
        o(Request.queues.default.length > 0).equals(true);
        Request.stopAllRequests();

        o(Request.queues.default.length).equals(0);
    });
});
