const o = require('ospec');

const Status = require('../src/models/status.js');

o.spec('Status Model', () => {
    o.specTimeout(10000);
    o('Initialize class', (done) => {
        Status.init('97A7F59E-C9D5-8A3C-D68A-2C3CE4C3874A').then(() => {
            o(Status.data.hasOwnProperty('author')).equals(true);
            o(Status.loading).equals(false);

            done();
        });
        o(Status.loading).equals(true);
    });
});
