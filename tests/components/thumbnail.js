const mq = require('mithril-query');
const o = require('ospec');

const Thumbnail = require('../../src/views/components/thumbnail.js');
const Mock = require('../mock.js')();

o.spec('Thumnail', () => {
    o('Standard thumbnail', () => {
        let out = mq(Thumbnail, {deviation: Mock.deviations.art.data});

        out.should.have('img');
        out.should.not.have('.literature');
        out.should.have('.user');
        out.should.have('.stats');
        out.should.not.have('.Thumbnail__Stats');
    });
    o('Literature thumbnail', () => {
        let out = mq(Thumbnail, {deviation: Mock.deviations.literature.data});

        out.should.not.have('img');
        out.should.have('.literature');
    });
    o('Hybrid thumbnail', () => {
        let out = mq(Thumbnail, {deviation: Mock.deviations.hybrid.data});

        out.should.have('img');
        out.should.have('.literature');
    });
    o('Large thumbnail', () => {
        let out = mq(Thumbnail, {deviation: Mock.deviations.art.data, size: 'Large'});

        out.should.have('.Thumbnail.Thumbnail__Large');
        out.should.have('.Thumbnail__Stats');
        out.should.not.have('.stats');
        out.should.have('h4');
    });
    o('No user', () => {
        let out = mq(Thumbnail, {deviation: Mock.deviations.art.data, nouser: true});

        out.should.not.have('.user');
    });
    o('No stats', () => {
        let out = mq(Thumbnail, {deviation: Mock.deviations.art.data, nostats: true});

        out.should.not.have('.stats');
        out.should.not.have('.Thumbnail__Stats');
    });
});
