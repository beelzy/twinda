const mq = require('mithril-query');
const o = require('ospec');

const Deviation = require('../../src/views/pages/deviation.js');

o.spec('Deviation', () => {
    o('Display deviation page', () => {
        let out = mq(Deviation, {id: '847AA897-5D7D-1115-0D68-537F60049873'});

        out.should.not.contain('Collection+');
    });
});
