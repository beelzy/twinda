const mq = require('mithril-query');
const o = require('ospec');

const Pagination = require('../../src/views/components/pagination.js');

o.spec('Pagination', () => {
    o('Standard pagination', () => {
        let out = mq(Pagination, {pages: 5, mobilePages: 3, total: 10, index: 0, changePageFn: () => {}});

        out.should.have(5, 'button.page');
    });
    o('Limited pagination', () => {
        let out = mq(Pagination, {pages: 5, mobilePages: 3, total: 2, index: 0, changePageFn: () => {}});

        out.should.have(2, 'button.page');
    });
});
