const mq = require('mithril-query');
const o = require('ospec');

const Entry = require('../../src/views/components/entry.js');
const Mock = require('../mock.js')();

o.spec('Entry', () => {
    o('Display journal entry', () => {
        let out = mq(Entry, {model: 'journal', entry: Mock.entries.journals.data.results[0]});

        out.should.contain('Daily Deviation Round-Up: May 2020!');
        out.should.have('.date');
        out.should.contain('JenFruzz');
        out.should.not.have('script');
        out.should.have(2, '.Icon__Thumbnail');
        out.should.contain('Read More');
    });
    o('Display status entry', () => {
        let out = mq(Entry, {model: 'status', entry: Mock.entries.statuses.data.results[0]});

        let date = new Date('2020-05-31T05:54:17-0700');

        out.should.contain(date.toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric'}));
        out.should.not.have('.date');
        out.should.contain('JenFruzz');
        out.should.not.have('script');
        out.should.have(1, '.Icon__Thumbnail');
        out.should.contain('View Comments');
    });
});
