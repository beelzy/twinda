const mq = require('mithril-query');
const o = require('ospec');

const Browse = require('../../src/views/pages/browse.js');

o.spec('Browse', () => {
    o('Display browse page', () => {
        let out = mq(Browse, {id: 'newest'});

        out.should.have('.Menu__Search');
        out.should.have('.Menu__Option');
        out.should.have('.List');
        out.should.have('.Page.Grid');
    });

    o('Display topic page', () => {
        let out = mq(Browse, {id: 'topic?topic=comics'});

        out.should.not.have('.Menu__Search');
    });
});
