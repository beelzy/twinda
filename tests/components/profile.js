const mq = require('mithril-query');
const o = require('ospec');

const Profile = require('../../src/views/pages/profile.js');

o.spec('Profile', () => {
    o('Display profile page', () => {
        let out = mq(Profile, {id: 'Xelku9'});

        out.should.have('.Profile__Tab');
        out.should.have('.Profile__Bar .Profile__Bar--ScrollWrapper');
        out.should.have(2, '.Profile__Column');
        out.should.have('.Card');
        out.should.contain('Featured');
        out.should.contain('Collections');
        out.should.contain('Watching');
        out.should.contain('Watchers');
        out.should.contain('About');
        out.should.contain('Statuses');
        out.should.contain('Comments');
    });
});
