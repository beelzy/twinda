const mq = require('mithril-query');
const o = require('ospec');

const Comments = require('../../src/views/components/comments.js');
const Pagination = require('../../src/models/pagination.js');
const Mock = require('../mock.js')();

o.spec('Comments', () => {
    o('Comments Loading', () => {
        let pagination = new Pagination.Instance();
        let out = mq(Comments, {thread: Mock.comments.data.thread, loading: true, page: pagination, replies: []});

        out.should.have('.Loading__Sticky');
    });
    o('Comments in scroll wrapper', () => {
        let pagination = new Pagination.Instance();
        let out = mq(Comments, {thread: Mock.comments.data.thread, fixedPagination: true, page: pagination, replies: []});

        out.should.have('.Comments__ScrollWrapper');
    });
});
