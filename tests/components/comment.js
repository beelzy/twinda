const mq = require('mithril-query');
const o = require('ospec');

const Comment = require('../../src/views/components/comment.js');
const Profile = require('../../src/models/profile.js');
const Mock = require('../mock.js')();

o.spec('Comment', () => {
    o('Display comment', () => {
        let out = mq(Comment, {comment: Mock.comments.data.thread[0], postCommentFn: Profile.postComment, replyEnabled: true});

        out.should.contain('mudballman912');
        let date = new Date('2020-05-29T03:50:38-0700');
        out.should.contain(date.toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric'}));
        out.should.have('[href="#!/profile/mudballman912"]');
        out.should.have('.Comment');
        out.should.not.have('.Reply');
    });
    o('Comments disabled', () => {
        let out = mq(Comment, {comment: Mock.comments.data.thread[0], postCommentFn: Profile.postComment, replyEnabled: false});

        out.should.have('.comments-disabled');
    });
});
