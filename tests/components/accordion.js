const mq = require('mithril-query');
const m = require('mithril');
const o = require('ospec');

const Accordion = require('../../src/views/components/accordion.js');

o.spec('Accordion', () => {
    o('Standard accordion', () => {
        let out = mq(Accordion, {disabled: false, content: m('.content', 'content div'), header: 'Accordion title'});

        out.should.have('.Accordion .content');
        out.should.contain('Accordion title');
        out.should.not.have('.opened');
    });
    o('Accordion with selector classes', () => {
        let out = mq(Accordion, {disabled: false, content: m('.content', 'content div'), header: 'Accordion title', headselector: '.custom-head-selector', selector: '.custom-content-selector'});

        out.should.have('.Accordion.custom-content-selector');
        out.should.have('.Accordion__Header.custom-head-selector');
    });
    o('Accordion disabled', () => {
        let out = mq(Accordion, {disabled: true, content: m('.content', 'content div'), header: 'Accordion title'});

        out.should.have('.disabled');
        out.should.have('.opened');
    });
    o('Accordion opened', () => {
        let out = mq(Accordion, {disabled: false, open: true, content: m('.content', 'content div'), header: 'Accordion title'});

        out.should.have('.opened');
    });
});
