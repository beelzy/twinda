const mq = require('mithril-query');
const o = require('ospec');

const Gallery = require('../../src/views/pages/gallery.js');

o.spec('Gallery', () => {
    o('Display gallery page', () => {
        let out = mq(Gallery, {user: 'Xelku9', model: 'gallery'});

        out.should.have('.Profile__Tab');
        out.should.have('.Profile__Bar .Buttons');
        out.should.contain('New');
        out.should.contain('Popular');
        out.should.contain('All');
        out.should.have('.Menu');
        out.should.have('.Menu__Option');
        out.should.contain('Gallery Folders');
    });
    o('Display collections page', () => {
        let out = mq(Gallery, {user: 'Xelku9', model: 'collections'});

        out.should.have('.Profile__Tab');
        out.should.not.contain('New');
        out.should.not.contain('Popular');
        out.should.contain('Collections Folders');
    });
    o('Display subfolder', () => {
        let out = mq(Gallery, {user: 'Xelku9', folderid: '640E5137-3390-4BD4-2D7A-3426C2F1398D', model: 'gallery'});

        out.should.have('.Profile__Tab');
        out.should.not.contain('New');
        out.should.not.contain('Popular');
        out.should.contain('All');
    })
});
