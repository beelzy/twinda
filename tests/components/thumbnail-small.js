const mq = require('mithril-query');
const o = require('ospec');

const Thumbnails = require('../../src/views/components/thumbnail-small.js');
const Mock = require('../mock.js')();

o.spec('Thumbnails Small', () => {
    o('Display art thumbnail', () => {
        let out = mq(Thumbnails, {deviation: Mock.deviations.art.data});

        out.should.have('.Thumbnails__Image');
        out.should.not.have('.literature');
    });
    o('Display literature thumbnail', () => {
        let out = mq(Thumbnails, {deviation: Mock.deviations.literature.data});

        out.should.have('.literature');
        out.should.not.have('.bg');
        out.should.not.have('.Thumbnails__Image');
    });
    o('Display hybrid thumbnail', () => {
        let out = mq(Thumbnails, {deviation: Mock.deviations.hybrid.data});
        
        out.should.have('.Thumbnails__Image')
        out.should.have('.bg');
        out.should.have('.literature');
    });
});
