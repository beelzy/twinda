const mq = require('mithril-query');
const o = require('ospec');

const Header = require('../../src/views/components/header.js');

o.spec('Header', () => {
    o('Display header', () => {
        let out = mq(Header);

        out.should.not.have('.notifications');
    });
});
