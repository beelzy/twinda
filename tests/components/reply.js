const mq = require('mithril-query');
const o = require('ospec');

const Reply = require('../../src/views/components/reply.js');

o.spec('Reply', () => {
    o('Not logged in', () => {
        let out = mq(Reply);

        out.should.have('span.login');
        out.should.not.have('textarea');
    });
});
