const mq = require('mithril-query');
const o = require('ospec');

const SVG = require('../../src/utils/svg.js');

o.spec('SVG Icons', () => {
    o('Generate icon', () => {
        let out = mq(SVG.icon('arrow'));

        out.should.have('svg');
        out.should.have('use');
    });
    o('Generate icon with css class', () => {
        let out = mq(SVG.icon('supergroup'));

        out.should.have('.brand');
    });
    o('Generate icon with size', () => {
        let out = mq(SVG.icon('gitlab'));

        out.should.have('[width="340px"]');
        out.should.have('[height="315px"]');
    });
});
