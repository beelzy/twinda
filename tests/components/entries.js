const mq = require('mithril-query');
const o = require('ospec');

const Entries = require('../../src/views/pages/entries.js');

o.spec('Entries', () => {
    o('Display journal page', () => {
        let out = mq(Entries, {user: 'Xelku9', model: 'journal'});

        out.should.have('.Profile__Tab');
        out.should.have('.Profile__Bar .Buttons');
        out.should.have('.Page');
        out.should.have('.Menu');
        out.should.have('.Menu__Option');
    });
    o('Display statuses page', () => {
        let out = mq(Entries, {user: 'Xelku9', model: 'status'});

        out.should.have('.Profile__Tab');
        out.should.not.have('.Profile__Bar .Buttons');
        out.should.contain('Statuses');
        out.should.have('.Page');
        out.should.have('.Menu');
        out.should.have('.Menu__Option');
    });
});
