const o = require('ospec');

const Browse = require('../src/models/browse.js');
const Request = require('../src/utils/request.js');

o.spec('Browse Model', () => {
    o.specTimeout(10000);
    o('Initialize class', () => {
        Browse.init();
        o(Browse.loading).equals(true);
        o(Request.queues.browse.length).equals(1);
        o(Browse.selectedMenuItem).equals('');
        o(Browse.key).equals('newest');
        o(Browse.index).equals(0);
    });
    o('Fetch page', (done) => {
        Browse.key = 'popular';
        Browse.fetchPage().then(() => {
            o(Browse.loading).equals(false);
            o(Browse.data.hasOwnProperty('results')).equals(true);
            o(Browse.page.index).equals(0);

            done();
        });
    });
    o('Fetch topics', (done) => {
        Browse.fetchTopics().then(() => {
            o(Browse.loadingTopics).equals(false);
            o(Browse.topics.length).equals(10);

            done();
        });
        o(Browse.loadingTopics).equals(true);
    });
});
