const o = require('ospec');

const Users = require('../src/models/users.js');
const Request = require('../src/utils/request.js');

o.spec('Users Class', () => {
    o.specTimeout(10000);
    o('Fetch users', (done) => {
        let users = new Users.Instance();
        users.fetch({slug: 'friends/Xelku9'}).then(() => {
            o(users.data.length > 0).equals(true);
            o(users.loading).equals(false);

            done();
        });
        o(users.loading).equals(true);
    });
    o('Fetch favorites', (done) => {
        let users = new Users.Instance();
        users.fetch({endpoint: Request.deviation, slug: 'whofaved', query: '?deviationid=847AA897-5D7D-1115-0D68-537F60049873'}).then(() => {
            o(users.data.length > 0).equals(true);

            done();
        });
    });
});
