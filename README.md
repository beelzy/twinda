# This Website Is Not DeviantArt

A clean and simple frontend for accessing DeviantArt using an API.

This app is made with Mithril and Express.

## Setup

Some configuration variables are saved in environment variables. In local development, you can put all the variables in `.env`. Please make sure the server you are using has the following variables:

* ENV - Set to `DEV` for local development, `STAGING` for staging and `PRODUCTION` for production.
* EXPRESS_SECRET - secret to provide for Express session cookies. A random hash is recommended.
* DA_SECRET - DeviantArt API `client_secret` of your app
* CLIENT_ID - DeviantArt API `client_id` of your app
* TEST_DOMAIN - The endpoint of the backend of this app for running tests. Ideally, it will just be the domain of the site (or http://0.0.0.0:5000 for local development)

For the DeviantArt API client secret and id, you will need to register your app to obtain them. You don't need to publish the app to be able to use it though. Then make sure to whitelist the domains you are going to use while you register your app. For local testing, make sure to also include the localhost url as well.

If you have a DeviantArt account, you can register for the API [here](https://www.deviantart.com/developers/register).

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and then clone this repo.

```sh
$ cd twinda
$ npm install
$ npm run watch
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

If you just need to rebuild the frontend, but not any Express server code, just run `npm run dev` to build the assets.

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```

You can replace the remote `heroku` with a branch name of your choosing.

## Deploying Elsewhere

Theoretically, it should work on any other server that supports Node. Take a look in the `Procfile` to find out what needs to run on deploy.

## Tests

There are some ospec tests for testing the frontend. In a local environment, make sure the server is running with `npm run watch` then run `npm run ospec`. For production use, you can use `npm run test` instead, since nodenv is not needed there. Please note that it is recommended to run tests in a separate testing environment instead of in a CI deploy release step so that the backend and frontend code are from the same commit. In Heroku, you can do this under the tests section instead of under release.

## Icons

* Loading icon from [Preloaders.net](https://icons8.com/preloaders/)
* [Entypo+](http://www.entypo.com) icons by Daniel Bruce, CC BY-SA 4.0

## License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
